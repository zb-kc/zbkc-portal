package com.zbkc.controller;


import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.CreateFileUtil;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.vo.FileVo;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.YwDevInfoService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * （新）业务办理——开发商基本信息表（移交用房申请） 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
@Slf4j
@RestController
@CrossOrigin("*")
@RequestMapping("/ywDevInfo")
public class YwDevInfoController {

    @Autowired
    private CreateFileUtil createFileUtil;

    @Autowired
    private YwDevInfoService ywDevInfoService;

    @ApiOperation("业务办理-开发商空间移交-文件上传-单个文件")
    @PostMapping("/fileUpload")
    public ResponseVO applyDataInsert(@RequestBody MultipartFile file , @RequestParam String ywHandlerDetailsId) throws BusinessException {
        log.info("业务办理-开发商空间移交-文件上传-单个文件:{}",new Gson().toJson(ywHandlerDetailsId));

        //保存文件
        FileVo vo = this.createFileUtil.saveFile(file);

        //保存文件路径
        SysFilePath1 sysFilePath1 = this.ywDevInfoService.saveSysFilePath(vo , ywHandlerDetailsId);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , sysFilePath1);
    }

    @ApiOperation("业务办理-开发商空间移交-文件下载-单个文件")
    @GetMapping("/downloadFileOne")
    public void downloadFileOne(HttpServletResponse response, @RequestParam String ywHandlerDetailsId) throws BusinessException {
        log.info("业务办理-开发商空间移交-文件下载-单个文件:{}",new Gson().toJson(ywHandlerDetailsId));

        this.ywDevInfoService.downloadFileOne(response , ywHandlerDetailsId);
    }

    @ApiOperation("业务办理-开发商空间移交-文件删除-单个文件")
    @GetMapping("/deleteFile")
    public ResponseVO deleteFile(@RequestParam("fileId") String fileId) throws BusinessException{
        log.info("业务办理-开发商空间移交-文件删除-单个文件:{}",new Gson().toJson(fileId));

        int record = this.ywDevInfoService.deleteFile(fileId);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @ApiOperation("业务办理-开发商空间移交-附件打包下载")
    @GetMapping("/downloadBatchByFile")
    public void downloadBatchByFile(@RequestParam String downloadFileIds,HttpServletResponse response) throws BusinessException {
        log.info("业务办理-开发商空间移交-附件打包下载:{}",new Gson().toJson(downloadFileIds));
        this.ywDevInfoService.downloadBatchByFile(response , downloadFileIds);
    }

    @ApiOperation("业务办理-开发商空间移交-文件下载-下载用房详情导入模板")
    @GetMapping("/downloadFileTemplate")
    public void downloadFileTemplate(HttpServletResponse response, @RequestParam String templateFileType) throws BusinessException {
        log.info("业务办理-开发商空间移交-文件下载-下载用房详情导入模板:{}",new Gson().toJson(templateFileType));

        this.ywDevInfoService.downloadFileTemplate(response , templateFileType);
    }

    @ApiOperation("业务办理-开发商空间移交-文件上传-导入用房详情模板")
    @PostMapping("/uploadFileTemplate")
    public ResponseVO uploadFileTemplate( @RequestBody @RequestParam("file") MultipartFile file , @RequestParam("ywDevInfoId") Long ywDevInfoId) throws BusinessException {
        log.info("业务办理-开发商空间移交-文件上传-导入用房详情模板:{}",new Gson().toJson(ywDevInfoId));

        int record = this.ywDevInfoService.uploadFileTemplate(file, ywDevInfoId);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

}

