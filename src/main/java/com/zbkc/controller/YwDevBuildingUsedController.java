package com.zbkc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 移交用房-用房详情表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
@Controller
@RequestMapping("/zbkc/ywDevBuildingUsed")
public class YwDevBuildingUsedController {

}

