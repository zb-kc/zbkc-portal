package com.zbkc.controller;


import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.DevSpaceHandoverDTO;
import com.zbkc.model.dto.GetHomeListDTO;
import com.zbkc.model.po.YwDevHandOver;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.YwDevHandOverService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 移交申请-开发商空间移交项目表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
@Api(tags = "开发商空间移交项目")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/ywDevHandOver")
public class YwDevHandOverController {
    @Autowired
    private YwDevHandOverService ywDevHandOverService;

    @ApiOperation(value = "开发商空间移交项目-新增")
    @PostMapping(value = "/add")
    public ResponseVO addDevSpaceHandoverPro(@RequestBody YwDevHandOver dto) throws BusinessException {
        log.info("开发商空间移交项目-新增:{}", new Gson().toJson(dto));
        return ywDevHandOverService.addDevSpaceHandoverPro(dto);
    }

    @ApiOperation(value = "开发商空间移交项目-修改")
    @PostMapping(value = "/upd")
    public ResponseVO updDevSpaceHandoverPro(@RequestBody DevSpaceHandoverDTO dto) throws BusinessException {
        log.info("开发商空间移交项目-修改:{}", new Gson().toJson(dto));
        return ywDevHandOverService.updDevSpaceHandoverPro(dto);
    }

    @ApiOperation(value = "开发商空间移交项目-查看详情")
    @GetMapping(value = "/details/{id}")
    public ResponseVO details(@PathVariable("id") Long id) throws BusinessException {
        log.info("开发商空间移交项目-查看详情:{}", new Gson().toJson(id));
        return ywDevHandOverService.details(id);
    }

    @ApiOperation(value = "开发商空间移交项目-首页")
    @PostMapping(value = "/list")
    public ResponseVO devSpaceHandoverProList(@RequestBody GetHomeListDTO dto) throws BusinessException {
        log.info("开发商空间移交项目-查看列表:{}", new Gson().toJson(dto));
        return ywDevHandOverService.devSpaceHandoverProList(dto);
    }

    @ApiOperation(value = "开发商空间移交项目-删除记录")
    @PostMapping(value = "/del/{id}")
    public ResponseVO delDevSpaceHandoverPro(@PathVariable("id") Long id) throws BusinessException {
        log.info("开发商空间移交项目-删除记录-id:{}", new Gson().toJson(id));
        return ywDevHandOverService.delDevSpaceHandoverPro(id);
    }
}

