package com.zbkc.controller;

import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.CreateFileUtil;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.SysFilePathMapper;
import com.zbkc.model.vo.FileVo;
import com.zbkc.model.vo.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;


/**
 * 上传文件
 * @author gmding
 * @date 2021/8/9
 */
@Api(tags = "文件服务")
@Controller
@RequestMapping("/fileUpload")
@Slf4j
public class FilesController {

    @Resource
    private CreateFileUtil createFileUtil;

    @Autowired
    private SysFilePathMapper sysFilePathMapper;

   /* @ApiOperation(value = "上传文件")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public ResponseVO upload(@RequestParam("file") MultipartFile file, AuthInfoVO vo) throws BusinessException {
        log.info( "文件:{},vo:{}",new Gson().toJson( vo ) );

        String files=createFileUtil.createFile("abc.log");
        log.info( "文件:{},vo:{}",files,new Gson().toJson( vo ) );
        return new ResponseVO( ErrorCodeEnum.SUCCESS,files);
    }*/


    @ApiOperation(value = "上传文件")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public ResponseVO upload( MultipartFile file) throws BusinessException {

        FileVo vo=createFileUtil.saveFile(file);

        log.info( "上传文件:{},vo:{}",new Gson().toJson( vo ) );
        return new ResponseVO( ErrorCodeEnum.SUCCESS,vo);
    }
}
