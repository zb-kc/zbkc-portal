package com.zbkc.controller;

import com.google.gson.Gson;
import com.zbkc.common.enums.CommonEnum;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.CreateFileUtil;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.po.YwApplyChangePO;
import com.zbkc.model.vo.*;
import com.zbkc.service.YwAgencyBusinessService;
import com.zbkc.service.YwHandoverDetailsSerivce;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/20
 */
@Api(tags = "合同服务")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/contact")
public class MhCompanyAuthorizationController {

    @Autowired
    private YwAgencyBusinessService ywAgencyBusinessService;

    @Autowired
    private YwHandoverDetailsSerivce ywHandoverDetailsSerivce;

    @javax.annotation.Resource
    private CreateFileUtil createFileUtil;


    @ApiOperation("合同服务-新增【测试用】-企业授权信息表")
    @PostMapping("/addRecord")
    public ResponseVO addRecord(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException {

        log.info("合同服务-新增【测试用】-企业授权信息表:{}",new Gson().toJson(ywAgencyBusinessDTO));

        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();
        BeanUtils.copyProperties(ywAgencyBusinessDTO , ywAgencyBusinessPO);
        int record = this.ywAgencyBusinessService.addYwAgencyBusiness(ywAgencyBusinessPO);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @ApiOperation("合同服务-分页-合同签订")
    @PostMapping("/contactSignList")
    public ResponseVO contactSignList(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        log.info("合同服务-合同签订:{}",new Gson().toJson(ywAgencyBusinessDTO));

        if(StringUtils.isEmpty(ywAgencyBusinessDTO.getSocialUniformCreditCode())){
            return new ResponseVO(ErrorCodeEnum.SOCIAL_UNITFORM_CREDIT_CODE_ERROR , ErrorCodeEnum.SOCIAL_UNITFORM_CREDIT_CODE_ERROR.getErrMsg());
        }

        if(ywAgencyBusinessDTO.getP() < 1){
            return new ResponseVO(ErrorCodeEnum.PAGE_NUMBER_ERROR , ErrorCodeEnum.PAGE_NUMBER_ERROR.getErrMsg());
        }

        PageVO<YwAgencyBusinessVO> ywAgencyBusinessVOPage = this.ywAgencyBusinessService.pageYwAgencyBusinessList(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , ywAgencyBusinessVOPage);
    }

    @ApiOperation("合同服务-按钮-合同签订申请")
    @PostMapping("/contactSignApply")
    public ResponseVO contactSignApply(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{
        log.info("合同服务-按钮-合同签订申请:{}",new Gson().toJson(ywAgencyBusinessDTO));

        if(ywAgencyBusinessDTO.getId() < 1){
            return new ResponseVO(ErrorCodeEnum.ID_ERROR , ErrorCodeEnum.ID_ERROR.getErrMsg());
        }
        YwAgencyBusinessDTO returnDTO = this.ywAgencyBusinessService.selectOne(String.valueOf(ywAgencyBusinessDTO.getId()));
        return new ResponseVO(ErrorCodeEnum.SUCCESS , returnDTO);
    }

    @ApiOperation("合同服务-我的合同")
    @PostMapping("/myContactList")
    public ResponseVO myContactList(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{
        log.info("合同服务-我的合同:{}",new Gson().toJson(ywAgencyBusinessDTO));

        if(StringUtils.isEmpty(ywAgencyBusinessDTO.getSocialUniformCreditCode())){
            return new ResponseVO(ErrorCodeEnum.SOCIAL_UNITFORM_CREDIT_CODE_ERROR , ErrorCodeEnum.SOCIAL_UNITFORM_CREDIT_CODE_ERROR.getErrMsg());
        }

        PageVO<MyContractVO> myContractVOList = this.ywAgencyBusinessService.selectBySocialUniformCreditCode(ywAgencyBusinessDTO);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , myContractVOList);
    }

    @ApiOperation("合同服务-申请记录")
    @PostMapping("/applyList")
    public ResponseVO applyList(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{
        log.info("合同服务-申请记录:{}",new Gson().toJson(ywAgencyBusinessDTO));

        if(StringUtils.isEmpty(ywAgencyBusinessDTO.getSocialUniformCreditCode())){
            return new ResponseVO(ErrorCodeEnum.SOCIAL_UNITFORM_CREDIT_CODE_ERROR , ErrorCodeEnum.SOCIAL_UNITFORM_CREDIT_CODE_ERROR.getErrMsg());
        }

        PageVO<MyApplyRecordVO> myContractVOList = this.ywAgencyBusinessService.selectApplyList(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , myContractVOList);
    }


    @ApiOperation("合同服务-按钮-合同详情")
    @PostMapping("/contactInfo")
    public ResponseVO contactInfo(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{
        log.info("合同服务-按钮-合同详情:{}",new Gson().toJson(ywAgencyBusinessDTO));

        if(ywAgencyBusinessDTO.getId() < 1){
            return new ResponseVO(ErrorCodeEnum.ID_ERROR , ErrorCodeEnum.ID_ERROR.getErrMsg());
        }
        
        YwContractVO ywContractVO = this.ywAgencyBusinessService.showOldContract(String.valueOf(ywAgencyBusinessDTO.getId()));
        return new ResponseVO(ErrorCodeEnum.SUCCESS , ywContractVO);
    }

    @ApiOperation("合同服务-按钮-提交申请")
    @PostMapping("/apply")
    public ResponseVO apply(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        log.info("合同服务-按钮-提交申请:{}",new Gson().toJson(ywAgencyBusinessDTO));

        if(ywAgencyBusinessDTO.getId() < 1){
            return new ResponseVO(ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND , ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND.getErrMsg());
        }

        YwAgencyBusinessPO apply = this.ywAgencyBusinessService.apply(ywAgencyBusinessDTO);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , apply);
    }

    @ApiOperation("合同服务-按钮-申请详情")
    @PostMapping("/applyInfo")
    public ResponseVO applyInfo(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        /**
         * 合同信息
         * 附件信息
         * 申请调整记录
         */
        log.info("合同服务-按钮-申请详情:{}",new Gson().toJson(ywAgencyBusinessDTO));

        if(ywAgencyBusinessDTO.getId() < 1){
            return new ResponseVO(ErrorCodeEnum.ID_ERROR , ErrorCodeEnum.ID_ERROR.getErrMsg());
        }
        
        ywAgencyBusinessDTO = this.ywAgencyBusinessService.selectOne(String.valueOf(ywAgencyBusinessDTO.getId()));

        return new ResponseVO(ErrorCodeEnum.SUCCESS , ywAgencyBusinessDTO);
    }

    @ApiOperation("合同服务-【测试用】-新增申请调整记录")
    @PostMapping("/addApplyChange")
    public ResponseVO addApplyChange(@RequestBody YwApplyChangePO ywApplyChangePO) throws BusinessException{
        log.info("合同服务-按钮-申请详情:{}",new Gson().toJson(ywApplyChangePO));

        Calendar instance = Calendar.getInstance();
        ywApplyChangePO.setOperatingTime(String.valueOf(instance.getTime().getTime()));
        int i = this.ywAgencyBusinessService.addApplyRecord(ywApplyChangePO);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , i);
    }

    @ApiOperation("合同服务-按钮-文件上传")
    @PostMapping("/fileUpload")
    public ResponseVO fileUpload(@RequestBody MultipartFile file , @RequestParam String id) throws BusinessException{
        log.info("合同服务-按钮-文件上传—文件id:{}",new Gson().toJson(id));

        //保存文件
        FileVo vo = this.createFileUtil.saveFile(file);
        if(vo.getCode() !=0 && !StringUtils.isEmpty(vo.getMsg())){
            ErrorCodeEnum value = ErrorCodeEnum.getValue(vo.getCode());
            return new ResponseVO(value, value.getErrMsg());
        }

        //保存文件路径
        SysFilePath1 sysFilePath1 = this.ywAgencyBusinessService.saveSysFilePath(vo , id);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , sysFilePath1);
    }

    @ApiOperation("合同服务-按钮-文件下载")
    @GetMapping("/fileDownload")
    public void fileDownload(HttpServletResponse response, @RequestParam String id) throws BusinessException{
        log.info("合同服务-按钮-文件下载—文件id:{}",new Gson().toJson(id));

        this.ywHandoverDetailsSerivce.downloadFileOne(response , id);
    }

    @ApiOperation("合同服务-按钮-文件删除")
    @GetMapping("/fileDelete")
    public ResponseVO fileDelete(@RequestParam("fileId") String dbFileId) throws BusinessException{
        log.info("业务办理-按钮-文件删除—文件id:{}",new Gson().toJson(dbFileId));
        int record = this.ywAgencyBusinessService.deleteFile(dbFileId);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @ApiOperation("合同服务-按钮-撤销申请")
    @PostMapping("/applyUndo")
    public ResponseVO applyUndo(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        /**
         * 工作流挂起/暂停
         */
        log.info("合同服务-按钮-撤销申请:{}",new Gson().toJson(ywAgencyBusinessDTO));

        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessService.selectYwAgencyBusinessById(ywAgencyBusinessDTO);
        if(ywAgencyBusinessPO.getRecordStatus().equals(CommonEnum.PROCESS_APPLY_UNDO)){
            return new ResponseVO(ErrorCodeEnum.DUPLICATE_ERROR , ErrorCodeEnum.DUPLICATE_ERROR.getErrMsg());
        }
        //业务状态为0  标识刚授权，应该看不到申请记录，如果看到则不正常
        if(ywAgencyBusinessPO.getRecordStatus().equals(CommonEnum.PROCESS_NEW_RECORD)){
            throw new BusinessException(ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND);
        }

        //校验是否能否撤回
        boolean canUndo = this.ywAgencyBusinessService.canUndo(ywAgencyBusinessDTO);
        if(!canUndo){
            return new ResponseVO(ErrorCodeEnum.CANEL_ERROR_PROGRESSING , ErrorCodeEnum.CANEL_ERROR_PROGRESSING.getErrMsg());
        }

        //执行撤回操作
        boolean flag = this.ywAgencyBusinessService.applyUndo(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , flag);
    }

    @ApiOperation("合同服务-【测试用】-根据参数获取待办的申请")
    @PostMapping("/selectUndoApply")
    public ResponseVO selectUndoApply(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{
        log.info("合同服务-【测试用】-根据参数获取待办的申请:{}",new Gson().toJson(ywAgencyBusinessDTO));
        List<String> businessIdList = this.ywAgencyBusinessService.selectUndoApply(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , businessIdList);
    }

    @ApiOperation("合同服务-入驻-收藏-合同-申请数量")
    @PostMapping("/myselfCenter")
    public ResponseVO myselfCenter(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{
        log.info("合同服务-入驻-收藏-合同-申请数量:{}",new Gson().toJson(ywAgencyBusinessDTO));
        Map<String, Integer> numberMap = this.ywAgencyBusinessService.countNumber(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , numberMap);
    }

}
