package com.zbkc.controller;

import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.GoverDTO;
import com.zbkc.model.dto.QueryWyInfoDTO;
import com.zbkc.model.dto.WyCollectionDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WyBasicInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 门户服务 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-09-09
 */
@Api(tags = "门户服务")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/wyBasic")
public class WyBasicInfoController {
    @Autowired
    private WyBasicInfoService wyBasicInfoService;

    @ApiOperation(value = "首页查询")
    @GetMapping(value = "/home/{socialUniformCreditCode}")
    public ResponseVO allPreContract(String socialUniformCreditCode)  {

        return wyBasicInfoService.homeList(socialUniformCreditCode);
    }

    @ApiOperation(value = "个人中心")
    @GetMapping(value = "/myEnter/{socialUniformCreditCode}")
    public ResponseVO myEnter(@PathVariable("socialUniformCreditCode")String socialUniformCreditCode)throws BusinessException {
        log.info("用户id:{}",new Gson().toJson(socialUniformCreditCode));
      return wyBasicInfoService.myEnter(socialUniformCreditCode);
    }

    @ApiOperation(value = "详情-物业简介")
    @GetMapping(value = "/details/{id}")
    public ResponseVO details(@PathVariable("id")Long id) throws BusinessException {
        return wyBasicInfoService.getDetails(id);
    }

    @ApiOperation(value = "政府物业")
    @PostMapping(value = "/goverWy")
    public ResponseVO goverWy(@RequestBody GoverDTO dto) throws BusinessException {
        return wyBasicInfoService.goverWy(dto);
    }

    @ApiOperation(value = "查看更多-列表")
    @PostMapping(value = "/showMore")
    public ResponseVO showMore(@RequestBody QueryWyInfoDTO queryWyInfoDTO)  {
        log.info("查看更多-列表:{}",new Gson().toJson(queryWyInfoDTO));
        return wyBasicInfoService.showMore(queryWyInfoDTO);
    }

    @ApiOperation(value = "收藏物业")
    @PostMapping(value = "/collect")
    public ResponseVO collect(@RequestBody WyCollectionDTO wyCollectionDTO)  {
        log.info("收藏物业:{}",new Gson().toJson(wyCollectionDTO));
        return wyBasicInfoService.collect(wyCollectionDTO);
    }

    @ApiOperation(value = "查看单个物业信息")
    @GetMapping(value = "/showOne/{wyId}/{socialUniformCreditCode}")
    public ResponseVO showOne(@PathVariable("wyId") Long id,@PathVariable("socialUniformCreditCode") String socialUniformCreditCode)  {
        log.info("查看单个物业信息:{}",new Gson().toJson(id +"," + socialUniformCreditCode));
        return wyBasicInfoService.selectOneWyInfo(id, socialUniformCreditCode);
    }

    @ApiOperation(value = "我的收藏")
    @PostMapping(value = "/myCollection")
    public ResponseVO selectMyCollection(@RequestBody QueryWyInfoDTO queryWyInfoDTO)  {
        log.info("我的收藏:{}",new Gson().toJson(queryWyInfoDTO));
        return wyBasicInfoService.selectMyCollection(queryWyInfoDTO);
    }
}
