package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 业务办理——移交用房详情
 * @author ZB3436.xiongshibao
 * @date 2021年10月12日
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("yw_handover_details")
public class YwHandoverDetailsPO implements Serializable {
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 材料名称
     */
    private String name;

    /**
     * 审核状态
     */
    private String status;

    /**
     * 材料审核指南
     */
    private String guidelines;

    /**
     * 附件文件id  关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long handoverDetailsImgId;

    /**
     *  业务办理——代办业务表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long ywAgencyBusinessId;

    /**
     * 材料补正意见
     */
    private String supplementDataOpinion;

    /**
     *  合同表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long yyContractId;

    /**
     * 文件id
     */
    @TableField(exist = false)
    private Long fileId;

    /**
     * 文件名称
     */
    @TableField(exist = false)
    private String fileName;

}