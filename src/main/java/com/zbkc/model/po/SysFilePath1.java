package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.configure.BasePathPatterns;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 系统上传文件路径表
 * </p>
 *
 * @author gmding
 * @since 2021-08-18
 */
@Data
public class SysFilePath1 implements Serializable {

    private static final long serialVersionUID=1L;
    /**
     * 主键id
     */

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 主表id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long dbFileId;
    /**
     * 文件存储路径
     * */
    private String path;
    /**
     *文件名称
     * */
    private String fileName;

    /**
     * 文件大小
     */
    private String fileSize;

    private String name;

    public String getPath() {
        return BasePathPatterns.urlPath+path;
    }
}
