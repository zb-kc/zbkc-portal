package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.vo.YyContractVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 业务办理——代办业务_移交用房申请表
 * @author ZB3436.xiongshibao
 * @date 2021-9-27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("yw_agency_business")
public class YwAgencyBusinessPO implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private long id;

    /**
     * 项目名称
     */
    private String proName;
    /**
     * 受理号
     */
    private String acceptCode;
    /**
     * 阶段名称
     */
    private String stageName;

    /**
     * 申请单位
     */
    private String applicantUnit;

    /**
     * 流程进度
     */
    private String process;

    /**
     * 申请时间
     */
    private Timestamp applicationTime;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 代办类型 1移交用房申请；2合同签署申请；3物业资产申报; 4 合同签署申请委非托运营类
     */
    private Integer agencyType;

    /**
     * 业务类型
     */
    private String type;

    /**
     * 联系人
     */
    private String contactName;

    /**
     * 联系电话
     */
    private String contactPhone;

    /**
     * 关联id
     */
    private Long relationId;


    /**
     * 网签类型 1新签 2续签
     */
    private String webSignType;

    /**
     * 申请人类型 1法人 2委托代理人
     */
    private Byte applyPeopleType;

    /**
     * 申请对象
     */
    private String applyObj;

    /**
     * 统一社会信用代码
     */
    private String socialUniformCreditCode;

    /**
     * 营业执照地址
     */
    private String businessLicenseAddress;

    /**
     * 物业名称
     */
    private String wyName;

    /**
     * 租赁范围
     */
    private String leaseArea;

    /**
     * 是否经济承诺贡献
     */
    private String isCommitment;
    /**
     * 经济贡献承诺
     */
    private String economicCommit;

    /**
     * 申请业务类型 1产业用房租赁 2商业用房租赁 3住宅用房租赁 4南山软件园租赁
     */
    private Byte businessType;

    /**
     * 续签类型 1三年内续签 2三年以上续签
     */
    private Byte renewalType;

    /**
     * 选择主合同id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long yyContactId;

    /**
     * 选择合同
     */
    private String selectContacts;

    /**
     * 备注
     */
    private String remark;

    /**
     * 流程状态 0刚授权 1企业申请 2企业撤回
     */
    private Byte recordStatus;

    /**
     * 审核意见
     */
    @TableField(exist = false)
    private String opinion;

    /**
     * 申请来源：1现场申请（管理端合同新增） 2线上申请（门户）
     */
    private String signSource;

    /**
     * 合同信息
     */
    @TableField(exist = false)
    private YyContractVO yyContractVO;

}
