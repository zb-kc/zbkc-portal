package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 移交用房-用房详情表
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class YwDevBuildingUsedPO implements Serializable {

    private static final long serialVersionUID=1L;

    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 物业种类-产业用房/社区配套
     */
    private String wyType;

    /**
     * 期数
     */
    private String periodNumber;

    /**
     * 栋号
     */
    private String buildingNumber;

    /**
     * 层数
     */
    private String floorNumber;

    /**
     * 房号
     */
    private String roomNumber;

    /**
     * 具体用途
     */
    private String concretePurpose;

    /**
     * 楼栋结构形式
     */
    private String buildingStructuralStyle;

    /**
     * 移交方式-有偿移交、无偿移交
     */
    private String handOverMode;

    /**
     * 计划移交时间
     */
    private Date handOverPlanTime;

    /**
     * 楼栋总高度
     */
    private Double buildingTotalHeight;

    /**
     * 楼栋总层数
     */
    private Integer buildingTotalFloor;

    /**
     * 用房建筑面积
     */
    private Double buildArea;

    /**
     * 本层层高
     */
    private Double thisFloorHeight;

    /**
     * 本层可用电梯数量
     */
    private Integer thisFloorLiftNumber;

    /**
     * 电梯荷载kg
     */
    private Double liftLoad;

    /**
     * 装修面净高
     */
    private Double renovatingHeight;

    /**
     * 设计荷载kn/m2
     */
    private Double designLoad;

    /**
     * 是否设置无障碍电梯 是/否
     */
    private String hasBarrierFreeElevator;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long ywDevInfoId;

}
