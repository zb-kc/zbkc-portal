package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author ZB3436.xiongshibao
 * @date 2021-10-22
 */
@Data
@TableName("yw_apply_change")
public class YwApplyChangePO {

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long ywAgencyBusinessId;

    private String operation;

    private String operatingTime;

}