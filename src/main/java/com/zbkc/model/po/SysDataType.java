package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * <p>
 * 数据字典类型表
 * </p>
 *
 * @author yangyan
 * @since 2021-08-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysDataType implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 字典类型名称
     */
    private String name;

    /**
     * 字典类型编号
     */
    private String code;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态(1:有效, 2:无效)
     */
    private Integer status;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 修改时间
     */
    private String updateTime;


}
