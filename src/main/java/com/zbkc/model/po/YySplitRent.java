package com.zbkc.model.po;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * <p>
 * 运营管理——合同管理_拆分租金单价表
 * </p>
 *
 * @author yangyan
 * @since 2021-09-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class YySplitRent implements Serializable {

    private static final long serialVersionUID=1L;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 运营管理——合同管理id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long yyContractId;

    /**
     * 出租物业地址 基础信息表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 物业名称
     */
    private String wyName;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 面积
     */
    private BigDecimal area;

    /**
     * 租金
     */
    private BigDecimal rent;

    private Timestamp createTime;

    /**
     * 1：未删除，2：已删除 默认1
     */
    private Integer isDel;


}
