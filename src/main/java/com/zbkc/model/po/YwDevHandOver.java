package com.zbkc.model.po;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 移交申请-开发商空间移交项目表
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class YwDevHandOver implements Serializable {

    private static final long serialVersionUID=1L;

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 项目编号
     */
    private String proCode;

    /**
     * 项目名称
     */
    private String proName;

    /**
     * 是否城市更新项目
     */
    private String isCityUpdate;

    /**
     * 是否工地用地
     */
    private String isBuildingWorks;

    /**
     * 是否有配件创新型产业用房
     */
    private String isBuildingInnovative;

    /**
     * 阶段0业务id 关联yw_agency_business表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long stepOneBusinessId;

    /**
     * 阶段1业务id 关联yw_agency_business表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long stepTwoBusinessId;

    /**
     * 阶段2业务id 关联yw_agency_business表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long stepThreeBusinessId;

    /**
     * 阶段3业务id 关联yw_agency_business表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long stepFourBusinessId;

    /**
     *统一社会信用代码/证件号码
     */
    private String code;

    /**
     *是否删除 1：未删除 2.已删除
     */
    private Integer isDel;
}
