package com.zbkc.model.vo;

import com.zbkc.model.po.WyBasicInfo;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/11/26
 */
@Data
public class WyBasicInfoVO extends WyBasicInfo {

    /**
     * 入驻人数
     */
    private Integer humanNum;
    /**
     * 入驻行业
     */
    private List<String> enterIndustry = new ArrayList<>();

    /**
     * 所在片区
     */
    private String location;

    /**
     * 园区标签
     */
    private List<String> parkTag = new ArrayList<>();
    /**
     * 街道名称
     */
    private String streetName;

    /**
     * 租金单价
     */
    private String rentPrice;

    /**
     * 物业标签
     */
    private String wyTag;

    /**
     * 物业种类
     */
    private String wyType;

    /**
     * 收藏人数
     */
    private Integer collectNumber;

    /**
     * 是否收藏
     */
    private Boolean isCollect = false;

    /**
     * 是否授权-代表着是否可以点击查看信息
     */
    private Boolean hasPermission = false;


}
