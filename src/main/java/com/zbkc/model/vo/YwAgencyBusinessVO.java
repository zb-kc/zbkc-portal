package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/9/27
 */
@Data
public class YwAgencyBusinessVO {

    @JsonSerialize(using= ToStringSerializer.class)
    private long id;

    /**
     * 业务名称
     */
    private String proName;

    /**
     * 业务类型
     */
    private String businessType;

    /**
     * 申请单位
     */
    private String applicantUnit;

    /**
     * 申请对象
     */
    private String applyObj;

    /**
     * 物业名称
     */
    private String wyName;

    /**
     * 是否经济承诺贡献
     */
    private String isCommitment;

    /**
     * 流程状态 0刚授权 1企业申请 2企业撤回
     */
    private Byte recordStatus = 0;

    /**
     * 流程进度
     */
    private String process;

    /**
     * 申请时间
     */
    private String applicationTime;

    /**
     * 申请人类型
     */
    private String applyPeopleType;
}
