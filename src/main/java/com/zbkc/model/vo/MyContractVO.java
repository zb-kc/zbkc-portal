package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/25
 */
@Data
public class MyContractVO {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id ;

    /**
     * 租赁物业名称
     */
    private String wyName;

    /**
     * 出租面积
     */
    private String rentalArea;

    /**
     * 签约类型
     */
    private String signType;

    /**
     * 申请人类型
     */
    private String applyPeopleType;

    /**
     * 是否经济贡献承诺
     */
    private String isEconomicCommitment;

    /**
     * 月租金
     */
    private String monthlyRent;

    /**
     * 租赁期限
     */
    private String leaseDuringTime;

    /**
     * 起租时间
     */
    private String startRentDate;
    /**
     * 止租时间
     */
    private String endRentDate;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long businessId;

}
