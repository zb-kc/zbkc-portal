package com.zbkc.model.vo;

import com.zbkc.model.po.WyBasicInfo;
import com.zbkc.model.po.WyFacilitiesInfo;
import lombok.Data;

import java.util.List;

/**
 * @author yangyan
 * @date 2021/10/21
 */
@Data
public class WyInfoDetailsVO {

    private WyBasicInfo wyBasicInfo;

    private List<WyFacilitiesInfo> wyFacilitiesInfo;

}
