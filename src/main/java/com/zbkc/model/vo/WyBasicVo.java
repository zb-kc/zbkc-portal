package com.zbkc.model.vo;

import com.zbkc.model.po.SysDataType;
import com.zbkc.model.po.WyBasicInfo;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author yangyan
 * @date 2021/10/20
 */
@Data
public class WyBasicVo {

    private List<Map<SysDataType, List<WyBasicInfoHomeVo>>> wyHome;

}
