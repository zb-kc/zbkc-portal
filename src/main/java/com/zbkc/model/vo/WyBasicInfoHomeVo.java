package com.zbkc.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangyan
 * @date 2021/11/17
 */
@Data
public class WyBasicInfoHomeVo {
    /**
     * 主键ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 物业名称
     */
    private String name;

    /**
     * 详细地址
     */
    private String detailAddr;

    /**
     * 建筑面积
     */
    private Double buildArea;


    /**
     * 入驻人数
     */
    private Integer humanNum;

    /**
     * 物业标签-政府物业/社会物业
     */
    private String wyTag;

    /**
     * 物业种类
     */
    private String wyType;

    /**
     * 所在街道
     */
    private String streetName;

    /**
     * 租金单价
     */
    private String rentPrice;

    /**
     * 入驻行业
     */
    private List<String> enterIndustry = new ArrayList<>();

    /**
     * 所在片区
     */
    private String location;

    /**
     * 园区标签
     */
    private List<String> parkTag = new ArrayList<>();

    /**
     * 收藏人数
     */
    private Integer collectNumber;

    /**
     * 是否收藏 true 是 false 否
     */
    private Boolean isCollect = false;

    /**
     * 是否授权-代表着是否可以点击查看信息
     */
    private Boolean hasPermission = false;

    /**
     * 使用登记id yy_use_reg_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long yyUserRegId;
}
