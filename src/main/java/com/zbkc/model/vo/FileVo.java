package com.zbkc.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author gmding
 * @Description 上传文件返回实体类
 * @date 2021-7-16
 * */
@Data
public class FileVo implements Serializable {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    private String fileUrl;
    private String fileName;
    private String filePath;
    private String fileSize;

    private int code;
    private String msg;

}
