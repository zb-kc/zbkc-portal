package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author caobiyang
 * @date 2021/12/23
 */
@Data
public class DevSpaceHandoverProVO {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 项目编号
     */
    private String proCode;

    /**
     * 项目名称
     */
    private String proName;

    /**
     * 是否有配件创新型产业用房
     */
    private String isBuildingInnovative;

    /**
     * 阶段0：创新型产业用房监管协议签署
     */
    private String stepOneBusinessProcess;

    /**
     * 阶段1：规划设计方案审核
     */
    private String stepTwoBusinessProcess;

    /**
     *  阶段2：意向移交
     */
    private String stepThreeBusinessProcess;

    /**
     * 阶段3：正式移交
     */
    private String stepFourBusinessProcess;

}
