package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/25
 */
@Data
public class MyApplyRecordVO {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 业务名称
     */
    private String proName;

    /**
     * 业务类型
     */
    private String businessType;

    /**
     * 承租方
     */
    private String lease;

    /**
     * 流程进度
     */
    private String process;

    /**
     * 申请时间
     */
    private String applicationTime;
}
