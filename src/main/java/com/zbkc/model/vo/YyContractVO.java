package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/27
 */
@Data
public class YyContractVO {

    @JsonSerialize(using= ToStringSerializer.class)
    Long id;
    /**
     * 申请人类型
     */
    int applicationType;
    /**
     * 承租方
     */
    @Length(max = 255 , message = "长度限制255个字符")
    String leaser;
    /**
     * 承租方证件类型 1社会统一信用代码，2身份证，3护照
     */
    int leaserType;
    /**
     * 承租方证件号码
     */
    @Length(max = 50 , message = "长度限制50个字符")
    String leaserCode;
    /**
     * 承租方联系方式
     */
    @Length(max = 20 , message = "长度限制20个字符")
    String leaserPhone;
    /**
     * 法定人代表
     */
    @Length(max = 100 , message = "长度限制100个字符")
    String legalPerson;
    /**
     *法定人联系方式
     */
    @Length(max = 20 , message = "长度限制20个字符")
    String legalPhone;
    /**
     *法定人代表证件类型  1身份证，2护照
     */
    int legalType;
    /**
     *法定人代表证件号码
     */
    @Length(max = 50 , message = "长度限制50个字符")
    String legalCode;
    /**
     *法定代表人联系地址
     */
    @Length(max = 255 , message = "长度限制255个字符")
    String legalAddress;
    /**
     *联系人
     */
    @Length(max = 100 , message = "长度限制100个字符")
    String contactPerson;
    /**
     *联系电话
     */
    @Length(max = 20 , message = "长度限制20个字符")
    String contactPhone;
    /**
     *委托代理人
     */
    @Length(max = 50 , message = "长度限制50个字符")
    String clientPerson;
    /**
     *委托代理人联系方式
     */
    @Length(max = 100 , message = "长度限制100个字符")
    String clientPhone;
    /**
     *委托代理人证件类型  1身份证，2护照
     */
    int clientType;
    /**
     *委托代理人证件号码
     */
    @Length(max = 50 , message = "长度限制50个字符")
    String clientCode;
    /**
     *委托代理人联系地址
     */
    @Length(max = 255 , message = "长度限制255个字符")
    String clientAddress;
    /**
     *企业信息备注
     */
    @Length(max = 255 , message = "长度限制255个字符")
    String companyInfoRemark;

    /**
     * 是否经济承诺贡献 1是 2无
     */
    private Integer isEconomicCommit;

    /**
     * 经济承诺贡献
     */
    @Length(max = 255 , message = "长度限制255个字符")
    private String economicCommit;

    /**
     * 备注
     */
    private String remark;

}
