package com.zbkc.model.vo;


import com.zbkc.model.po.WyBasicInfo;
import lombok.Data;

import java.util.List;

/**
 * @author yangyan
 * @date 2021/10/21
 */
@Data
public class MyEnterVO {

//    /**
//     * 企业信息
//     */
//  private SysUser sysUser;

    /**
     * 物业信息
     */
    private List<WyBasicInfo> wyBasicInfo;

    /**
     * 总数目
     */
    private Integer total;
}
