package com.zbkc.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/11/25
 */
@Data
public class WyCollectionDTO {

    /**
     * 统一社会信用代码
     */
    private String socialUniformCreditCode;

    /**
     * 物业id
     */
    @TableId(value = "id",type = IdType.ASSIGN_ID   )
    private Long wyBasicInfoId;

    /**
     * 收藏类型 1 收藏 2取消收藏
     */
    private Byte collectType;
}
