package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

/**
 * 门户-物业列表筛选项
 * @author ZB3436.xiongshibao
 * @date 2021/11/25
 */
@Data
public class QueryWyInfoDTO {


    /**
     * 所在辖区
     */
    private String jurisdictionOfArea;

    /**
     * 靠近地铁
     */
    private List<String> closeToSubwayNumber;

    /**
     * 看房模式 不限 VR 360 720
     */
    private List<String> lookModel;

    /**
     * 最小面积
     */
    private Integer minBuildArea;
    /**
     * 最大面积
     */
    private Integer maxBuildArea;

    /**
     * 租售方式
     */
    private List<String> rentalModel;

    /**
     * 产权性质
     */
    private String propertyRights;

    /**
     * 最小租金单价
     */
    private Integer minRentPrice;

    /**
     * 最大租金单价
     */
    private Integer maxRentPrice;

    /**
     * 最小售价
     */
    private Integer minOfferPrice;

    /**
     * 最大售价
     */
    private Integer maxOfferPrice;

    /**
     * 入驻行业
     */
    private List<String> enterIndustry;

    /**
     * 园区配套
     */
    private List<String> supportingPark;

    /**
     * 统一社会信用代码
     */
    private String socialUniformCreditCode;
    private Integer p;
    private Integer size;
    /**
     * 0 默认按照id 升序
     * 1根据租金升序
     * 2根据租金降序
     * 3根据热门房源降序
     */
    private Integer sort;

    /**
     * 物业名称
     */
    private String wyName;

    /**
     * 物业ids
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private List<Long> wyIds;

}
