package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author yangyan
 * @date  2021/10/22
 */
@Data
public class GoverDTO {

    /**
     * 物业名称
     */
    private  String wyName;
    /**
     * 所在辖区
     */
    private  String Jurisdiction;

    /**
     * 附近地铁
     */
    private String nearSubway;

    /**
     * 看房模式
     */
    private String lookMethood;

    /**
     * 面积
     */
    private Double area;

    /**
     * 租售方式
     */
    private  String sellMethod;

    /**
     * 产权性质
     */
    private String property;

    /**
     * 租金单价
     */
    private BigDecimal rentUnitPrice;

    /**
     * 售价
     */
    private BigDecimal sellPrice;

    /**
     * 入驻行业
     */
    private String settledIndusty;

    /**
     * 园区配套
     */
    private String parkSupport;

    /**
     * 页码
     */
    private Integer p;
    /**
     * 行数
     */
    private Integer size;
    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

}
