package com.zbkc.model.dto;


import com.zbkc.model.po.SysFilePath;
import com.zbkc.model.po.YyContract;
import com.zbkc.model.po.YySplitRent;
import lombok.Data;

import java.util.List;


/**
 * @author yangyan
 */
@Data
public class YyContractDTO {
  /**
   * 主键id
   */
  private Long id;
    /**
     * 传输对象
     */
   private YyContract info;

   /**
    * 拆分列表
    * */
   private List<YySplitRent>rentList;


    /**
     * 甲方持有房屋证明文件
     */
   private List<SysFilePath> certificationDocumentsFile;

    /**
     * 法人身份证复印件
     */
    private List<SysFilePath> legalPersonFile;

    /**
     * 营业执照复印件
     */
    private List<SysFilePath> businessLicenseFile;

    /**
     * 法人代表证明书
     */
    private List<SysFilePath> legalRepresentativeFile;

    /**
     * 法人授权委托书
     */
    private List<SysFilePath> legalPersonPowerFile;

    /**
     * 经办人身份证复印件
     */
    private List<SysFilePath> handlingPersonCardFile;

    /**
     * 南山区政策性产业用房入驻通知书
     */
    private List<SysFilePath> settlementNoticeFile;

    /**
     * 南山区产业用房建设和管理工作领导小组会议纪要
     */
    private List<SysFilePath> meetingMinutesFile;

    /**
     *房屋建筑面积总表
     */
    private List<SysFilePath> buildingAreaFile;

    /**
     * 房屋建筑面积分户汇总表及房屋建筑面积分户位置图
     */
    private List<SysFilePath> locationMapFile;

    /**
     * 有经济贡献率的企业的经济贡献承诺书
     */
    private List<SysFilePath> commitmentFile;

    /**
     * 会议纪要
     */
    private List<SysFilePath> meetingFile;

    /**
     * 租金价格评估报告
     */
    private List<SysFilePath> rentReportFile;

    /**
     * 身份证复印件
     */
    private List<SysFilePath> personFile;

    /**
     * 租金价格评估报告南山数字文化产业基地入驻通知书
     */
    private List<SysFilePath> rentNoticeFile;

    /**
     * 南山数字文化产业基地入驻通知书
     */
    private List<SysFilePath> digitalNoticeFile;

    /**
     * 合同
     */
    private List<SysFilePath> contractFile;

}
