package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysFilePath1;
import lombok.Data;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/13
 */
@Data
public class YwHandoverDetailsDTO {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 材料名称
     */
    private String name;

    /**
     * 审核状态
     */
    private String status;

    /**
     * 材料审核指南
     */
    private String guidelines;

    /**
     * 附件文件id  关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long handoverDetailsImgId;

    /**
     *  业务办理——代办业务表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long ywAgencyBusinessId;

    /**
     * 材料补正意见
     */
    private String supplementDataOpinion;

    /**
     * 附件信息表
     */
    private List<SysFilePath1> sysFilePathList;

    /**
     * 文件大小
     */
    private String fileSize;
}
