package com.zbkc.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.YwApplyChangePO;
import com.zbkc.model.po.YwHandoverDetailsPO;
import com.zbkc.model.vo.YyContractVO;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/9/27
 */
@Data
public class YwAgencyBusinessDTO {

    @JsonSerialize(using= ToStringSerializer.class)
    private long id;

    /**
     * 项目名称
     */
    private String proName;
    /**
     * 受理号
     */
    private String acceptCode;
    /**
     * 阶段名称
     */
    private String stageName;

    /**
     * 申请单位
     */
    @Length(max = 255 , message = "长度限制255个字符")
    private String applicantUnit;

    /**
     * 流程进度
     */
    private String process;

    /**
     * 申请时间
     */
    private Timestamp applicationTime;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 当前页码
     */
    private int p;
    /**
     * 每页行数
     */
    private int size;
    /**
     * 查询-开始时间
     */
    @JsonIgnore
    private Timestamp queryStartTime;
    /**
     * 查询-结束时间
     */
    @JsonIgnore
    private Timestamp queryEndTime;

    /**
     * 代办类型 1移交用房申请；2物业资产申报; 3合同签署申请
     */
    private Integer agencyType;

    /**
     * 申请业务类型 1产业用房租赁 2商业用房租赁 3住宅用房租赁 4南山软件园租赁
     */
    private String businessType;

    /**
     * 网签类型 1新签 2续签
     */
    private String webSignType;

    /**
     * 申请对象
     */
    private String applyObj;

    /**
     * 统一社会信用代码
     */
    @Length(max = 100 , message = "长度限制100个字符")
    private String socialUniformCreditCode;

    /**
     * 营业执照地址
     */
    @Length(max = 255 , message = "长度限制255个字符")
    private String businessLicenseAddress;

    /**
     * 物业名称
     */
    @Length(max = 255 , message = "长度限制255个字符")
    private String wyName;

    /**
     * 租赁范围
     */
    @Length(max = 255 , message = "长度限制255个字符")
    private String leaseArea;

    /**
     * 是否经济承诺贡献
     */
    private String isCommitment;

    /**
     * 续签类型 1三年内续签 2三年以上续签
     */
    private Byte renewalType;

    /**
     * 选择主合同id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @JsonIgnore
    private Long yyContactId;

    /**
     * 选择合同
     */
    @JsonIgnore
    private String selectContacts;

    /**
     * 备注
     */
    @Length(max = 1000 , message = "长度限制1000个字符")
    private String remark;

    /**
     * 流程状态 0刚授权 1企业申请 2企业撤回 3管理员撤销授权
     */
    private Byte recordStatus = 0;

    /**
     * 申请材料
     */
    private List<YwHandoverDetailsPO> ywHandoverDetailsPOList;
    /**
     * 申请调整记录
     */
    private List<YwApplyChangePO> ywApplyChangePOList;

    /**
     * 合同信息
     */
    private YyContractVO yyContractVO;

    /**
     * 经济承诺贡献
     */
    @Length(max = 255 , message = "长度限制255个字符")
    private String economicCommit;

}
