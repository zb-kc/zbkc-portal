package com.zbkc.model.dto;

import lombok.Data;

/**
 * @author caobiyang
 * @date  2021/12/23
 */
@Data
public class GetHomeListDTO {

    /**
     * 页码
     */
    private Integer p;

    /**
     * 行数
     */
    private Integer size;

    /**
     * 项目名称
     */
    private String proName;

    /**
     * 是否有配件创新型产业用房
     */
    private String isBuildingInnovative;
}
