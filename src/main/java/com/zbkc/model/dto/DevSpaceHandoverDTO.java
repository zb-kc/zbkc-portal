package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author caobiyang
 * @date  2021/12/23
 */
@Data
public class DevSpaceHandoverDTO {
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 项目名称
     */
    private String proName;

    /**
     * 是否城市更新项目
     */
    private String isCityUpdate;

    /**
     * 是否工地用地
     */
    private String isBuildingWorks;

    /**
     * 是否有配件创新型产业用房
     */
    private String isBuildingInnovative;

}
