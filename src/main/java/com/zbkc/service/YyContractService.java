package com.zbkc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.po.YyContract;

/**
 * <p>
 * 运营管理——合同管理 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-13
 */
public interface YyContractService extends IService<YyContract> {


}