package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zbkc.mapper.YwAgencyBusinessMapper;
import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.vo.YwProcessInfoVO;
import com.zbkc.service.ActivitiBaseService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.HistoricProcessInstanceEntity;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 工作流服务类 实现
 * @author ZB3436.xiongshibao
 * @since 2021-9-16
 */
@Service("activitiBaseService")
@Slf4j
public class ActivitiBaseServiceImpl implements ActivitiBaseService {

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ProcessEngineConfiguration processEngineConfiguration;

    @Autowired
    private YwAgencyBusinessMapper ywAgencyBusinessMapper;


    @Override
    public List<YwAgencyBusinessPO> selectProcessCompleteList(String socialUniformCreditCode) {

        QueryWrapper<YwAgencyBusinessPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("social_uniform_credit_code" , socialUniformCreditCode);
        List<YwAgencyBusinessPO> ywAgencyBusinessPOS = this.ywAgencyBusinessMapper.selectList(queryWrapper);
        List<String> businessIdList = new ArrayList<>();
        if(ywAgencyBusinessPOS.isEmpty()){
            return null;
        }

        for (YwAgencyBusinessPO ywAgencyBusinessPO : ywAgencyBusinessPOS) {
            String businessId = String.valueOf(ywAgencyBusinessPO.getId());
            businessIdList.add(businessId);
        }
        List<String> completedBusinessId = new ArrayList<>();
        for (String businessId : businessIdList) {
            HistoricProcessInstanceEntity historicProcessInstance = (HistoricProcessInstanceEntity)this.historyService.createHistoricProcessInstanceQuery().processInstanceBusinessKey(businessId).singleResult();
            if(null == historicProcessInstance){
                continue;
            }

            String endActivityId = historicProcessInstance.getEndActivityId();
            String deleteReason = historicProcessInstance.getDeleteReason();
            if(StringUtils.isNotEmpty(endActivityId) && StringUtils.isEmpty(deleteReason)){
                completedBusinessId.add(businessId);
            }
        }
        List<YwAgencyBusinessPO> newBusinessList = ywAgencyBusinessPOS.stream().filter(e -> completedBusinessId.contains(String.valueOf(e.getId()))).collect(Collectors.toList());

        return newBusinessList;
    }

    @Override
    public void startProcessInstance(YwAgencyBusinessPO ywAgencyBusinessPO) {
        if (0 == ywAgencyBusinessPO.getAgencyType()){
            log.info("业务办理-申请类型为空，业务id："+ ywAgencyBusinessPO.getBusinessType());
        }

        String bpmnStr = "";

        switch (ywAgencyBusinessPO.getAgencyType()){
            case 1:
                //移交用房申请
                bpmnStr = "deliveryHouseApply1";
                break;
            case 3:
                //合同签署-运营类
                bpmnStr = "contractSignApply2";
                break;
            case 4:
                //合同签署-非运营类
                bpmnStr = "contractSignApply1";
                break;
            case 2:
                //物业资产申报
                bpmnStr = "propertyApply";
                break;
            default:
                break;
        }

        List<ProcessDefinition> list = this.repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(bpmnStr)
                .orderByProcessDefinitionVersion()
                .desc()
                .list();

        if(list.isEmpty()){
            throw new RuntimeException("流程定义为空，请输入正确的流程定义名称或联系管理员");
        }

        ProcessDefinition processDefinition = list.get(0);

        ProcessInstance processInstance = this.runtimeService.startProcessInstanceByKey(processDefinition.getKey(), String.valueOf(ywAgencyBusinessPO.getId()));

        HistoricTaskInstance historicTaskInstance = this.historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(processInstance.getId())
                .singleResult();

        this.taskService.setOwner(historicTaskInstance.getId()  , String.valueOf(ywAgencyBusinessPO.getUserId()));

        log.info("业务ID：" + ywAgencyBusinessPO.getId() +"已开启流程，流程名称：" + processDefinition.getName());
    }


    @Override
    public void addAgencyBusinessRecord(YwAgencyBusinessPO ywAgencyBusinessPO) {
        //开启工作流
        this.startProcessInstance(ywAgencyBusinessPO);
        //完成申请节点
        YwProcessInfoVO ywProcessInfoVO = new YwProcessInfoVO();
        ywProcessInfoVO.setBusinessId(String.valueOf(ywAgencyBusinessPO.getId()));
        this.completeTask(String.valueOf(ywAgencyBusinessPO.getId()) , ywAgencyBusinessPO.getOpinion());
        //设置流程状态
        String processStatus = this.getProcessStatus(String.valueOf(ywAgencyBusinessPO.getId()));
        ywAgencyBusinessPO.setProcess(processStatus);

        //新增申请记录
        int id = this.ywAgencyBusinessMapper.updateById(ywAgencyBusinessPO);
    }

    @Override
    public void completeTask(String businessKey, String opinion) {
        Task task = this.taskService.createTaskQuery()
                .processInstanceBusinessKey(businessKey)
                .singleResult();

        if(StringUtils.isNotEmpty(opinion)){
            this.taskService.addComment(task.getId() , task.getProcessInstanceId() , opinion);
        }

        this.taskService.complete(task.getId());
    }

    @Override
    public String getProcessStatus(String businessId) {

        Task task = this.taskService.createTaskQuery()
                .processInstanceBusinessKey(businessId)
                .singleResult();

        if(null == task){
            List<HistoricTaskInstance> list = this.historyService.createHistoricTaskInstanceQuery()
                    .processInstanceBusinessKey(businessId)
                    .orderByTaskId()
                    .desc()
                    .list();

            if(list.isEmpty()){
                return null;
            }

            return list.get(0).getName();
        }

        return task.getName();
    }

    @Override
    public boolean selectByBusinessId(String businessId) {

        ProcessInstance processInstance = this.runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(businessId).singleResult();
        if(null != processInstance){
            return true;
        }

        return false;
    }

    @Override
    public boolean suspendProcessInstance(String businessId) {
        ExecutionEntity processInstance =  (ExecutionEntity)this.runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(businessId).singleResult();
        if(null == processInstance){
            return false;
        }

        //suspensionState 1为激活中 2位挂起
        int suspensionState = processInstance.getSuspensionState();
        if(suspensionState == 1){
            this.runtimeService.suspendProcessInstanceById(processInstance.getId());
        }

        return true;
    }

    @Override
    public boolean activateProcessInstance(YwAgencyBusinessPO ywAgencyBusinessPO) {

        String businessId = String.valueOf(ywAgencyBusinessPO.getId());

        ExecutionEntity processInstance =  (ExecutionEntity)this.runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(businessId).singleResult();
        if(null == processInstance){
            return false;
        }

        //suspensionState 1为激活中 2位挂起
        int suspensionState = processInstance.getSuspensionState();
        if(suspensionState != 1){
            //情况1 挂起时需要激活
            this.runtimeService.activateProcessInstanceById(processInstance.getId());
        }else{
            //情况2 重新申请时 需流转到下一节点
            this.completeTask(businessId, "重新申请");

            ywAgencyBusinessPO.setProcess("重新申请");
        }

        return true;
    }

    @Override
    public String getSecondActivityId(YwAgencyBusinessDTO ywAgencyBusinessDTO) {
        String secondActivityId = "";
        ProcessInstance processInstance = this.runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(String.valueOf(ywAgencyBusinessDTO.getId())).singleResult();
        String processDefinitionId = processInstance.getProcessDefinitionId();

        BpmnModel model = this.repositoryService.getBpmnModel(processDefinitionId);
        if(model != null) {
            Collection<FlowElement> flowElements = model.getMainProcess().getFlowElements();
            Map<String, String> nodeMap = new HashMap<>();
            String firstFlow = "";
            List<SequenceFlow> allFlow = new ArrayList<>();
            Map<String, Integer> flowNumberMap = new HashMap<>();

            for (FlowElement e : flowElements) {
                if (e instanceof UserTask) {
                    List<SequenceFlow> incomingFlows = ((UserTask) e).getIncomingFlows();
                    List<SequenceFlow> outgoingFlows = ((UserTask) e).getOutgoingFlows();
                    allFlow.addAll(incomingFlows);
                    allFlow.addAll(outgoingFlows);
                }
            }

            for (SequenceFlow flow : allFlow) {
                String sourceRef = flow.getSourceRef();
                String targetRef = flow.getTargetRef();
                nodeMap.put(sourceRef, targetRef);

                Integer number = flowNumberMap.get(sourceRef);
                if (null == number) {
                    number = 0;
                }
                number += 1;
                flowNumberMap.put(sourceRef, number);

                Integer number2 = flowNumberMap.get(targetRef);
                if (null == number2) {
                    number2 = 0;
                }
                number2 += 1;
                flowNumberMap.put(targetRef, number2);
            }

            for (Map.Entry<String, Integer> entry : flowNumberMap.entrySet()) {
                if (entry.getValue().equals(1) && nodeMap.containsKey(entry.getKey())) {
                    firstFlow = entry.getKey();
                    break;
                }
            }

            secondActivityId = nodeMap.get(nodeMap.get(firstFlow));
        }
        return secondActivityId;
    }
}
