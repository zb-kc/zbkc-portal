package com.zbkc.service.impl;

import com.zbkc.model.po.YwDevBuildingUsedPO;
import com.zbkc.mapper.YwDevBuildingUsedMapper;
import com.zbkc.service.YwDevBuildingUsedService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 移交用房-用房详情表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
@Service
public class YwDevBuildingUsedServiceImpl extends ServiceImpl<YwDevBuildingUsedMapper, YwDevBuildingUsedPO> implements YwDevBuildingUsedService {

}
