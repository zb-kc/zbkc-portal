package com.zbkc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.mapper.WyCollectionMapper;
import com.zbkc.model.po.WyCollectionPO;
import com.zbkc.service.WyCollectionService;
import org.springframework.stereotype.Service;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/11/25
 */
@Service
public class WyCollectionServiceImpl extends ServiceImpl<WyCollectionMapper, WyCollectionPO> implements WyCollectionService {

}
