package com.zbkc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.mapper.SysFilePathMapper;
import com.zbkc.mapper.YyContractMapper;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.WyBasicVo;
import com.zbkc.service.YyContractService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.concurrent.*;

/**
 * <p>
 * 运营管理——合同管理 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-13
 */
@Service
@Slf4j
public class YyContractServiceImpl extends ServiceImpl<YyContractMapper, YyContract> implements YyContractService {

    @Autowired
    private ToolUtils toolUtils;
    @Autowired
    private YyContractMapper yyContractMapper;
    @Autowired
    private SysFilePathMapper sysFilePathMapper;

    private final static ExecutorService executorService = new ThreadPoolExecutor(4, 20, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());



}