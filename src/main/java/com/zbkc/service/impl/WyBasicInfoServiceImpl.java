package com.zbkc.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.enums.CommonEnum;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.WyBasicInfoMapper;
import com.zbkc.mapper.WyCollectionMapper;
import com.zbkc.mapper.YyContractWyRelationMapper;
import com.zbkc.model.dto.GoverDTO;
import com.zbkc.model.dto.QueryWyInfoDTO;
import com.zbkc.model.dto.WyCollectionDTO;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.*;
import com.zbkc.service.ActivitiBaseService;
import com.zbkc.service.WyBasicInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 物业基础信息表	 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-10-20
 */
@Service
public class WyBasicInfoServiceImpl extends ServiceImpl<WyBasicInfoMapper, WyBasicInfo> implements WyBasicInfoService {
    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;
    @Autowired
    private ActivitiBaseService activitiBaseService;
    @Autowired
    private YyContractWyRelationMapper yyContractWyRelationMapper;
    @Autowired
    private WyCollectionMapper wyCollectionMapper;

    @Override
    public ResponseVO homeList(String socialUniformCreditCode) {
        WyBasicVo vo = new WyBasicVo();

        //暂时屏蔽
//        List<Long> hasPermissions = this.yyContractWyRelationMapper.selectWyIdBySocialCode(socialUniformCreditCode);

        List<SysDataType> keyList = wyBasicInfoMapper.keyList();

        //根据政府物业、国企物业、集体物业、社会物业 分组取前8条
        List<WyBasicInfoHomeVo> wyBasicInfoHomeList = this.wyBasicInfoMapper.selectWyBasicInfoHomeList();

        //将查询数据放到map中
        Map<String , List<WyBasicInfoHomeVo>> wyBasicInfoMap = new HashMap<>(4);
        if(null != wyBasicInfoHomeList){
            for (WyBasicInfoHomeVo wyBasicInfoHomeVo : wyBasicInfoHomeList) {
                //设置入驻人数
                Integer humanNum = this.wyBasicInfoMapper.countEnterNumber2(wyBasicInfoHomeVo.getId());
                wyBasicInfoHomeVo.setHumanNum(humanNum);
                //暂时屏蔽 设置是否有权限访问详情
//                if(hasPermissions.contains(wyId)){
                        wyBasicInfoHomeVo.setHasPermission(true);
//                }

                List<WyBasicInfoHomeVo> tempList = wyBasicInfoMap.get(wyBasicInfoHomeVo.getWyTag());
                if(null == tempList){
                    tempList = new ArrayList<>();
                }
                tempList.add(wyBasicInfoHomeVo);
                wyBasicInfoMap.put(wyBasicInfoHomeVo.getWyTag() , tempList);
            }
        }

        //将上面的map重新组装，返回
        HashMap<SysDataType, List<WyBasicInfoHomeVo>> map = new HashMap<>();
        for (SysDataType key:keyList) {
            String sysDataTypeName = key.getName();
            map.put(key,wyBasicInfoMap.get(sysDataTypeName));
        }

        List<Map<SysDataType, List<WyBasicInfoHomeVo>>> list = new ArrayList<>();
        list.add(map);
        vo.setWyHome(list);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,list);
    }

    @Override
    public ResponseVO myEnter(String socialUniformCreditCode) {

        List<YwAgencyBusinessPO> ywAgencyBusinessPOList = this.activitiBaseService.selectProcessCompleteList(socialUniformCreditCode);
        if(null == ywAgencyBusinessPOList || ywAgencyBusinessPOList.isEmpty()){
            return null;
        }
        List<Long> contractIdList = ywAgencyBusinessPOList.stream().map(YwAgencyBusinessPO::getRelationId).collect(Collectors.toList());

        MyEnterVO vo = new MyEnterVO();

        for (Long contractId : contractIdList) {
            List<Long> wyIds = yyContractWyRelationMapper.wyBasicIdsList(contractId);

            List<WyBasicInfo> list = new ArrayList<>();
            for (Long wyId : wyIds) {
                WyBasicInfo wyBasicInfo = wyBasicInfoMapper.selectBywyId(wyId);
                list.add(wyBasicInfo);
            }
            vo.setWyBasicInfo(list);
            vo.setTotal(list.size());

        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO getDetails(Long id) {

        WyInfoDetailsVO vo=new WyInfoDetailsVO();
        WyBasicInfo info=wyBasicInfoMapper.getDetailsList( id );
        vo.setWyBasicInfo(info);
        List<WyFacilitiesInfo> facilies = wyBasicInfoMapper.selectFaciliesById(id);
        vo.setWyFacilitiesInfo(facilies);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }

    @Override
    public ResponseVO goverWy(GoverDTO dto) {
        List<WyBasicInfo> wyBasicInfoList = wyBasicInfoMapper.goverWy(dto);



        return new ResponseVO(ErrorCodeEnum.SUCCESS, wyBasicInfoList);
    }

    @Override
    public ResponseVO showMore(QueryWyInfoDTO queryWyInfoDTO) {

        if(null == queryWyInfoDTO.getP() || null == queryWyInfoDTO.getSize() || queryWyInfoDTO.getP() < 0 || queryWyInfoDTO.getSize() < 0){
            throw new BusinessException(ErrorCodeEnum.PAGE_NUMBER_ERROR);
        }

        if(StringUtils.isEmpty(queryWyInfoDTO.getSocialUniformCreditCode())){
            throw new BusinessException(ErrorCodeEnum.SOCIAL_UNITFORM_CREDIT_CODE_ERROR);
        }

        Integer currentPage = queryWyInfoDTO.getP();
        int queryNumber = (currentPage - 1) * queryWyInfoDTO.getSize();
        queryWyInfoDTO.setP(queryNumber);

        List<WyBasicInfoHomeVo> wyBasicInfoHomeVos = this.wyBasicInfoMapper.showMore(queryWyInfoDTO);

        List<Long> hasPermissions = this.yyContractWyRelationMapper.selectWyIdBySocialCode(queryWyInfoDTO.getSocialUniformCreditCode());

        List<Long> wyIdBySocialCode = this.wyCollectionMapper.selectWyIdBySocialCode(queryWyInfoDTO.getSocialUniformCreditCode(), null, null);
        /**
         * 查询入驻人数---
         * 判断是否有访问权限---
         * 是否收藏---
         */
        for (WyBasicInfoHomeVo wyBasicInfoHomeVo : wyBasicInfoHomeVos) {
            Long wyId = wyBasicInfoHomeVo.getId();
            Integer countEnterNumber = this.wyBasicInfoMapper.countEnterNumber(wyId);
            wyBasicInfoHomeVo.setHumanNum(countEnterNumber);

            //暂时屏蔽
//            if(hasPermissions.contains(wyId)){
                wyBasicInfoHomeVo.setHasPermission(true);
//            }
            if(wyIdBySocialCode.contains(wyId)){
                wyBasicInfoHomeVo.setIsCollect(true);
            }
        }

        Integer total = this.wyBasicInfoMapper.showMoreTotal(queryWyInfoDTO);

        PageVO<WyBasicInfoHomeVo> pageVO = new PageVO<>();
        pageVO.setTotal(total);
        pageVO.setList(wyBasicInfoHomeVos);
        pageVO.setP(currentPage);
        pageVO.setSize(queryWyInfoDTO.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS , pageVO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO collect(WyCollectionDTO wyCollectionDTO) {

        if(StringUtils.isEmpty(wyCollectionDTO.getSocialUniformCreditCode()) ||
                null == wyCollectionDTO.getWyBasicInfoId() ||
                wyCollectionDTO.getWyBasicInfoId() < 1   ||
                null == wyCollectionDTO.getCollectType()
        ){
            throw new BusinessException(ErrorCodeEnum.NULLERROR);
        }

        int record = 0;

        //收藏
        if(wyCollectionDTO.getCollectType().equals(CommonEnum.WY_COLLECT)){

            Integer hasRecord = this.wyCollectionMapper.selectByCodeAndWyId(wyCollectionDTO.getSocialUniformCreditCode(), wyCollectionDTO.getWyBasicInfoId());
            if(hasRecord > 0){
                throw new BusinessException(ErrorCodeEnum.COLLECTION_HAS);
            }

            WyCollectionPO wyCollectionPO = new WyCollectionPO();
            BeanUtils.copyProperties(wyCollectionDTO , wyCollectionPO);
            wyCollectionPO.setId(IdWorker.getId());
            wyCollectionPO.setCreateTime(new Date());
            record = this.wyCollectionMapper.insert(wyCollectionPO);
        }
        //取消收藏
        if(wyCollectionDTO.getCollectType().equals(CommonEnum.WY_COLLECT_UNDO)){

            Integer hasRecord = this.wyCollectionMapper.selectByCodeAndWyId(wyCollectionDTO.getSocialUniformCreditCode(), wyCollectionDTO.getWyBasicInfoId());
            if(hasRecord == 0){
                throw new BusinessException(ErrorCodeEnum.COLLECTION_UNDO);
            }

            QueryWrapper<WyCollectionPO> deleteQuery = new QueryWrapper<>();
            deleteQuery.eq("wy_basic_info_id" , wyCollectionDTO.getWyBasicInfoId());
            deleteQuery.eq("social_uniform_credit_code" , wyCollectionDTO.getSocialUniformCreditCode());
            record = this.wyCollectionMapper.delete(deleteQuery);
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @Override
    public ResponseVO selectOneWyInfo(Long id, String socialUniformCreditCode) {
        if(null == id){
            throw new BusinessException(ErrorCodeEnum.NULLERROR);
        }

        if(StringUtils.isEmpty(socialUniformCreditCode)){
            throw new BusinessException(ErrorCodeEnum.SOCIAL_UNITFORM_CREDIT_CODE_ERROR);
        }

        //暂时屏蔽
//        List<Long> hasPermissionList = this.yyContractWyRelationMapper.selectWyIdBySocialCode(socialUniformCreditCode);
//        if(null == hasPermissionList || !hasPermissionList.contains(id)){
//            throw new BusinessException(ErrorCodeEnum.PERMISSIONS_ERROR);
//        }

        Integer record = this.wyCollectionMapper.selectByCodeAndWyId(socialUniformCreditCode, id);

        WyInfoDetailsVO detailsVO =new WyInfoDetailsVO();
        WyBasicInfoVO info=wyBasicInfoMapper.selectOneWyInfo( id );
        info.setHasPermission(true);
        if(record > 0 ){
            info.setIsCollect(true);
        }

        detailsVO.setWyBasicInfo(info);
        List<WyFacilitiesInfo> facilies = wyBasicInfoMapper.selectFaciliesById(id);
        detailsVO.setWyFacilitiesInfo(facilies);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , detailsVO);
    }

    @Override
    public ResponseVO selectMyCollection(QueryWyInfoDTO queryWyInfoDTO) {

        if(StringUtils.isEmpty(queryWyInfoDTO.getSocialUniformCreditCode())){
            throw new BusinessException(ErrorCodeEnum.SOCIAL_UNITFORM_CREDIT_CODE_ERROR);
        }

        if(null == queryWyInfoDTO.getP() || queryWyInfoDTO.getP() < 0){
            throw new BusinessException(ErrorCodeEnum.PAGE_NUMBER_ERROR);
        }

        Integer currentPage = queryWyInfoDTO.getP();
        Integer currentSize = queryWyInfoDTO.getSize();
        Integer queryNumber = (currentPage - 1) * queryWyInfoDTO.getSize();
        //根据信用代码 查询收藏的物业信息
        List<Long> wyIds = this.wyCollectionMapper.selectWyIdBySocialCode(queryWyInfoDTO.getSocialUniformCreditCode() , queryNumber, queryWyInfoDTO.getSize());
        Integer total = this.wyCollectionMapper.selectWyIdBySocialCodeTotal(queryWyInfoDTO.getSocialUniformCreditCode());

        //查看是否有权限
        List<Long> hasPermissions = this.yyContractWyRelationMapper.selectWyIdBySocialCode(queryWyInfoDTO.getSocialUniformCreditCode());

        queryWyInfoDTO.setP(null);
        queryWyInfoDTO.setSize(null);
        queryWyInfoDTO.setWyIds(wyIds);

        //根据物业id 查询物业信息
        List<WyBasicInfoHomeVo> collectionWyInfoList = this.wyBasicInfoMapper.showMore(queryWyInfoDTO);
        for (WyBasicInfoHomeVo wyBasicInfoHomeVo : collectionWyInfoList) {
            Long wyId = wyBasicInfoHomeVo.getId();

            Integer countEnterNumber = this.wyBasicInfoMapper.countEnterNumber(wyId);
            wyBasicInfoHomeVo.setHumanNum(countEnterNumber);

            if(hasPermissions.contains(wyId)){
                wyBasicInfoHomeVo.setHasPermission(true);
            }
            wyBasicInfoHomeVo.setIsCollect(true);
        }

        PageVO<WyBasicInfoHomeVo> pageVO = new PageVO<>();
        pageVO.setTotal(total);
        pageVO.setP(currentPage);
        pageVO.setSize(currentSize);
        pageVO.setList( collectionWyInfoList);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , pageVO);
    }
}
