package com.zbkc.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.enums.CommonEnum;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.CreateFileUtil;
import com.zbkc.common.utils.FileUtils;
import com.zbkc.common.utils.ReadExcelUtils;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.*;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.FileVo;
import com.zbkc.service.YwDevInfoService;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * （新）业务办理——开发商基本信息表（移交用房申请） 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
@Service
public class YwDevInfoServiceImpl extends ServiceImpl<YwDevInfoMapper, YwDevInfo> implements YwDevInfoService {

    @Autowired
    private SysFilePathMapper sysFilePathMapper;

    @Autowired
    private YwHandoverDetailsMapper ywHandoverDetailsMapper;

    @Autowired
    YwAgencyBusinessMapper ywAgencyBusinessMapper;

    @Autowired
    private CreateFileUtil createFileUtil;

    @Value( "${file.win-name}")
    private String winUrl;

    @Value( "${file.linux-name}" )
    private String linuxUrl;

    @Autowired
    private YwDevBuildingUsedMapper ywDevBuildingUsedMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SysFilePath1 saveSysFilePath(FileVo vo, String relationId) {
        //修改申请材料审核状态表
        YwHandoverDetailsPO ywHandoverDetailsPO = new YwHandoverDetailsPO();
        ywHandoverDetailsPO.setId(Long.parseLong(relationId));

        //根据relationId查询，有则修改 无则新增
        QueryWrapper<SysFilePath> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("db_file_id", relationId);
        SysFilePath sysFilePathTemp = this.sysFilePathMapper.selectOne(queryWrapper);
        if (null != sysFilePathTemp) {
            sysFilePathTemp.setFileName(vo.getFileName());
            sysFilePathTemp.setPath(vo.getFilePath());
            sysFilePathTemp.setFileSize(vo.getFileSize());
            this.sysFilePathMapper.updateById(sysFilePathTemp);

            ywHandoverDetailsPO.setStatus(CommonEnum.FILE_STATUS_TODO);
        } else {
            sysFilePathTemp = new SysFilePath();
            sysFilePathTemp.setId(IdWorker.getId());
            sysFilePathTemp.setFileName(vo.getFileName());
            sysFilePathTemp.setPath(vo.getFilePath());
            sysFilePathTemp.setDbFileId(Long.parseLong(relationId));
            sysFilePathTemp.setFileSize(vo.getFileSize());
            this.sysFilePathMapper.addSysFilePath(sysFilePathTemp);
        }

        ywHandoverDetailsPO.setHandoverDetailsImgId(sysFilePathTemp.getId());
        ywHandoverDetailsPO.setSupplementDataOpinion("");
        this.ywHandoverDetailsMapper.updateById(ywHandoverDetailsPO);

        SysFilePath1 sysFilePath1 = new SysFilePath1();
        BeanUtils.copyProperties(sysFilePathTemp, sysFilePath1);
        return sysFilePath1;
    }

    @Override
    public void downloadFileOne(HttpServletResponse response, String ywHandlerDetailsId) {
        QueryWrapper<SysFilePath> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("db_file_id" , ywHandlerDetailsId);
        SysFilePath sysFilePath = this.sysFilePathMapper.selectOne(queryWrapper);

        String path = sysFilePath.getPath();
        //TODO 文件路径获取问题
        String preFix = createFileUtil.isWindows() ? winUrl : linuxUrl;
        String urlString = preFix + "/" + path;
        InputStream is = null;
        try {
            if(!urlString.startsWith("http")) {
                File file = new File(urlString);
                is = new BufferedInputStream(new FileInputStream(file));
            }else{
                // 构造URL
                java.net.URL url = new java.net.URL(urlString);
                // 打开连接
                URLConnection con = url.openConnection();
                // 输入流
                is = con.getInputStream();
            }

            FileUtils.buildFile(sysFilePath.getFileName() , is , response);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != is){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int deleteFile(String fileId) {
        int i = 0;
        SysFilePath sysFilePath = this.sysFilePathMapper.selectById(fileId);
        if (null != sysFilePath) {
            String path = sysFilePath.getPath();
            String preFix = createFileUtil.isWindows() ? winUrl : linuxUrl;

            CreateFileUtil.deleteFile(preFix + "/" + path);
            i = this.sysFilePathMapper.deleteById(fileId);
        }

        return i;
    }

    @Override
    public void downloadBatchByFile(HttpServletResponse response, String downloadFileIds) {
        //封装需要下载的文件流转化为byte[]
        Map<String, byte[]> files = new HashMap<>();

        List<SysFilePath> sysFilePaths = null;
        if (StringUtils.isNotEmpty(downloadFileIds)) {
            List<Long> queryList = new ArrayList<>();
            String[] downloadFileIdsArr = downloadFileIds.split(",");
            for (String s : downloadFileIdsArr) {
                queryList.add(Long.parseLong(s));
            }

            QueryWrapper<SysFilePath> queryWrapper = new QueryWrapper<>();
            queryWrapper.in("db_file_id", queryList);
            sysFilePaths = this.sysFilePathMapper.selectList(queryWrapper);
        }

        if (null == sysFilePaths || sysFilePaths.isEmpty()) {
            return;
        }

        try {
            for (SysFilePath sysFilePath : sysFilePaths) {
                String fileName = sysFilePath.getFileName();
                String path = sysFilePath.getPath();
                //TODO 文件路径获取问题
                String preFix = createFileUtil.isWindows() ? winUrl : linuxUrl;

                byte[] remoteFile = FileUtils.getRemoteFile(preFix + "/" + path);

                files.put(fileName, remoteFile);
            }

            //调用下载
            FileUtils.downloadBatchByFile(response, files, "batchFile");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @SneakyThrows
    public void downloadFileTemplate(HttpServletResponse response, String templateFileType) {
        if(templateFileType.isEmpty()){
            throw new BusinessException(ErrorCodeEnum.NULLERROR);
        }

        String fileName = "";

        //判断模板类型
        switch (templateFileType){
            case "devImport":
                //用房详情导入模板
                fileName = "classpath:templateFile/devImport.xlsx";
                break;
            default:
                break;
        }

        InputStream inputStream = new FileInputStream(ResourceUtils.getFile(fileName));
        FileUtils.buildFile(fileName , inputStream , response);

    }

    @Override
    @SneakyThrows
    public int uploadFileTemplate(MultipartFile file, Long ywDevInfoId) {

        if(null == ywDevInfoId || ywDevInfoId < 0){
            throw new BusinessException(ErrorCodeEnum.ID_IS_NULL);
        }

        int record = 0;
        //读取excel文件内容
        List<Map<String, String>> objectList = ReadExcelUtils.readExcelInfo(file);

        if(null != objectList && objectList.size() > 0 ){
            //将excel读取到的内容转为object
            List<YwDevBuildingUsedPO> ywDevBuildingUsedList = new ArrayList<>(objectList.size());
            for (Map<String, String> attributeMap : objectList) {
                YwDevBuildingUsedPO ywDevBuildingUsed = JSON.parseObject(JSON.toJSONString(attributeMap), YwDevBuildingUsedPO.class);
                ywDevBuildingUsed.setYwDevInfoId(ywDevInfoId);
                ywDevBuildingUsedList.add(ywDevBuildingUsed);
            }
            //批量更新
            this.ywDevBuildingUsedMapper.insertBatch(ywDevBuildingUsedList);
        }

        return record;
    }
}
