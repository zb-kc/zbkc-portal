package com.zbkc.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.enums.*;
import com.zbkc.common.utils.CreateFileUtil;
import com.zbkc.common.utils.FileUtils;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.*;
import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.*;
import com.zbkc.service.ActivitiBaseService;
import com.zbkc.service.YwAgencyBusinessService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/9/27
 */
@Service
public class YwAgencyBusinessServiceImpl extends ServiceImpl<YwAgencyBusinessMapper, YwAgencyBusinessPO> implements YwAgencyBusinessService {

    @Autowired
    YwAgencyBusinessMapper ywAgencyBusinessMapper;

    @Autowired
    YyContractMapper yyContractMapper;

    @Autowired
    private SysFilePathMapper sysFilePathMapper;

    @Autowired
    private CreateFileUtil createFileUtil;

    @Value( "${file.win-name}")
    private String winUrl;
    @Value( "${file.linux-name}" )
    private String linuxUrl;

    @Autowired
    private SysDataTypeMapper sysDataTypeMapper;

    @Autowired
    private YwHandoverDetailsMapper ywHandoverDetailsMapper;

    @Autowired
    private YwApplyChangeMapper ywApplyChangeMapper;

    @Autowired
    private ActivitiBaseService activitiBaseService;

    @Resource
    private ToolUtils toolUtils;

    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;

    @Autowired
    private YyContractWyRelationMapper yyContractWyRelationMapper;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private WyCollectionMapper wyCollectionMapper;

    @Override
    public int addYwAgencyBusiness(YwAgencyBusinessPO ywAgencyBusinessPO) {
        return this.ywAgencyBusinessMapper.insert(ywAgencyBusinessPO);
    }

    @Override
    public int removeYwAgencyBusiness(YwAgencyBusinessPO ywAgencyBusinessPO) {
        return this.ywAgencyBusinessMapper.deleteById(ywAgencyBusinessPO.getId());
    }

    @Override
    public int updateYwAgencyBusiness(YwAgencyBusinessDTO ywAgencyBusinessDTO) {

        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();
        BeanUtils.copyProperties(ywAgencyBusinessDTO, ywAgencyBusinessPO);

        return this.ywAgencyBusinessMapper.updateById(ywAgencyBusinessPO);
    }

    @Override
    public YwAgencyBusinessPO selectYwAgencyBusinessById(YwAgencyBusinessDTO ywAgencyBusinessDTO) {
        return this.ywAgencyBusinessMapper.selectById(ywAgencyBusinessDTO.getId());
    }

    @Override
    public PageVO<YwAgencyBusinessVO> pageYwAgencyBusinessList(YwAgencyBusinessDTO ywAgencyBusinessDTO) {

        if(ywAgencyBusinessDTO.getP()<=0){
            return null;
        }

        int expectPage = ywAgencyBusinessDTO.getP();
        int queryStartRowNumber = (ywAgencyBusinessDTO.getP() - 1) * ywAgencyBusinessDTO.getSize();

        ywAgencyBusinessDTO.setP(queryStartRowNumber);
        List<YwAgencyBusinessVO> list = this.ywAgencyBusinessMapper.selectYwAgencyBusinessByPage(ywAgencyBusinessDTO);
        int total = this.ywAgencyBusinessMapper.selectYwAgencyBusinessByPageCount(ywAgencyBusinessDTO);

        PageVO pageVO = new PageVO();
        pageVO.setList(list);
        pageVO.setTotal(total);
        pageVO.setP(expectPage);
        pageVO.setSize(ywAgencyBusinessDTO.getSize());
        return pageVO;
    }

    @Override
    public YwAgencyBusinessDTO selectOne(String businessId) {
        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessMapper.selectOneIdByBusinessId(businessId);

        YwAgencyBusinessDTO ywAgencyBusinessDTO = new YwAgencyBusinessDTO();
        BeanUtils.copyProperties(ywAgencyBusinessPO , ywAgencyBusinessDTO);
        ywAgencyBusinessDTO.setEconomicCommit(null == ywAgencyBusinessPO.getYyContractVO() ? null : ywAgencyBusinessPO.getYyContractVO().getEconomicCommit());

        //申请材料
        QueryWrapper<YwHandoverDetailsPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("yw_agency_business_id" , businessId);
        List<YwHandoverDetailsPO> ywHandoverDetailsPOList = this.ywHandoverDetailsMapper.selectByBusinessId(Long.parseLong(businessId));
        ywAgencyBusinessDTO.setYwHandoverDetailsPOList(ywHandoverDetailsPOList);

        //申请调整记录
        QueryWrapper<YwApplyChangePO> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("yw_agency_business_id" , businessId);
        List<YwApplyChangePO> ywApplyChangePOList = this.ywApplyChangeMapper.selectList(queryWrapper2);
        ywAgencyBusinessDTO.setYwApplyChangePOList(ywApplyChangePOList);

        return ywAgencyBusinessDTO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SysFilePath1 saveSysFilePath(FileVo vo, String ywHandlerDetailsId) {

        long relationId = Long.parseLong(ywHandlerDetailsId);
        //根据ywHandlerDetailsId查询，有则修改 无则新增
        QueryWrapper<SysFilePath> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("db_file_id" , ywHandlerDetailsId);
        SysFilePath sysFilePathTemp = this.sysFilePathMapper.selectOne(queryWrapper);
        if(null != sysFilePathTemp){
            sysFilePathTemp.setFileName(vo.getFileName());
            sysFilePathTemp.setPath(vo.getFilePath());
            sysFilePathTemp.setFileSize(vo.getFileSize());
            this.sysFilePathMapper.updateById(sysFilePathTemp);
        }else{
            sysFilePathTemp = new SysFilePath();
            sysFilePathTemp.setFileName(vo.getFileName());
            sysFilePathTemp.setPath(vo.getFilePath());
            sysFilePathTemp.setDbFileId(relationId);
            sysFilePathTemp.setId(IdWorker.getId());
            sysFilePathTemp.setFileSize(vo.getFileSize());
            this.sysFilePathMapper.addSysFilePath(sysFilePathTemp);
        }

        //更新关联表
        Long sysFilePathTempId = sysFilePathTemp.getId();
        YwHandoverDetailsPO ywHandoverDetailsPO = this.ywHandoverDetailsMapper.selectById(ywHandlerDetailsId);
        ywHandoverDetailsPO.setHandoverDetailsImgId(sysFilePathTempId);
        ywHandoverDetailsPO.setSupplementDataOpinion("");
        ywHandoverDetailsPO.setStatus(CommonEnum.FILE_STATUS_TODO);
        this.ywHandoverDetailsMapper.updateById(ywHandoverDetailsPO);

        YyContract yyContract = this.yyContractMapper.selectById(ywHandoverDetailsPO.getYyContractId());
        String name = ywHandoverDetailsPO.getName();

        ContractFileNameEnum[] values = ContractFileNameEnum.values();
        ContractFileNameEnum contractFileNameEnum = null;
        for (ContractFileNameEnum value : values) {
            if(value.getFileName().equals(name)){
                contractFileNameEnum = value;
                break;
            }
        }

        if(null != contractFileNameEnum && null != yyContract){
            switch (contractFileNameEnum.getId()){
                case 1:
                    yyContract.setResidentNoticeFilePathId(relationId);
                    break;
                case 2:
                    yyContract.setBusinessLicenseFilePathId(relationId);
                    break;
                case 3:
                    yyContract.setLegalPersonFilePathId(relationId);
                    break;
                case 4:
                    yyContract.setLegalRepresentativeFilePathId(relationId);
                    break;
                case 5:
                    yyContract.setShareholderFilePathId(relationId);
                    break;
                case 6:
                    yyContract.setLegalPersonPowerFilePathId(relationId);
                    break;
                case 7:
                    yyContract.setHandlingPersonCardFilePathId(relationId);
                    break;
                case 17:
                    yyContract.setRenewalSignFilePathId(relationId);
                    break;
                default:
                    break;
            }
            this.yyContractMapper.updateById(yyContract);
        }

        SysFilePath1 sysFilePath1 = new SysFilePath1();
        BeanUtils.copyProperties(sysFilePathTemp , sysFilePath1);
        return sysFilePath1;
    }

    @Override
    public void downloadBatchByFile(HttpServletResponse response, String downloadFileIds) {
        //封装需要下载的文件流转化为byte[]
        Map<String, byte[]> files = new HashMap<>() ;

        List<SysFilePath> sysFilePaths = null;
        if(StringUtils.isNotEmpty(downloadFileIds)){
            List<Long> queryList = new ArrayList<>();
            String[] downloadFileIdsArr = downloadFileIds.split(",");
            for (String s : downloadFileIdsArr) {
                queryList.add(Long.parseLong(s));
            }

            QueryWrapper<SysFilePath> queryWrapper = new QueryWrapper<>();
            queryWrapper.in("db_file_id" , queryList);
            sysFilePaths = this.sysFilePathMapper.selectList(queryWrapper);
        }

        if (null == sysFilePaths || sysFilePaths.isEmpty()){
            return;
        }

        try {

            for (SysFilePath sysFilePath : sysFilePaths) {
                String fileName = sysFilePath.getFileName();
                String path = sysFilePath.getPath();
                //TODO 文件路径获取问题
                String preFix = createFileUtil.isWindows() ? winUrl : linuxUrl;

                byte[] remoteFile = FileUtils.getRemoteFile(preFix + "/" + path);

                files.put(fileName,  remoteFile);
            }

            //调用下载
            FileUtils.downloadBatchByFile(response, files, "download");

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public YwContractVO showOldContract(String businessId) {
        //业务状态为0  标识刚授权，应该看不到申请记录，如果看到则不正常
        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessMapper.selectById(businessId);
        if(null != ywAgencyBusinessPO && ywAgencyBusinessPO.getRecordStatus().equals(CommonEnum.PROCESS_NEW_RECORD)){
            throw new BusinessException(ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND);
        }

        YwContractVO ywContractVO = this.yyContractMapper.selectYwContractIdByBusinessId(businessId);
        if(null == ywContractVO){
            return null;
        }

        //申请材料
        QueryWrapper<YwHandoverDetailsPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("yw_agency_business_id" , businessId);
        List<YwHandoverDetailsPO> ywHandoverDetailsPOList = this.ywHandoverDetailsMapper.selectByBusinessId(Long.parseLong(businessId));
        ywContractVO.setYwHandoverDetailsPOList(ywHandoverDetailsPOList);

        List<SysDataType> houseOwnerShipList = this.sysDataTypeMapper.selectAllByCodeLive("house_ownership");
        ywContractVO.setHouseOwnerShipList(houseOwnerShipList);

        List<SysDataType> houseCredentialsType = this.sysDataTypeMapper.selectAllByCodeLive("house_proof");
        ywContractVO.setPartAHouseCredentialsList(houseCredentialsType);


        return ywContractVO;
    }

    @Override
    public int addApplyRecord(YwApplyChangePO ywApplyChangePO) {
        return this.ywApplyChangeMapper.insert(ywApplyChangePO);
    }

    @Override
    public PageVO<MyContractVO> selectBySocialUniformCreditCode(YwAgencyBusinessDTO ywAgencyBusinessDTO) {

        List<YwAgencyBusinessPO> ywAgencyBusinessPOList = this.activitiBaseService.selectProcessCompleteList(ywAgencyBusinessDTO.getSocialUniformCreditCode());
        if(null == ywAgencyBusinessPOList || ywAgencyBusinessPOList.isEmpty()){
            return null;
        }

        List<Long> contractIdList = ywAgencyBusinessPOList.stream().map(YwAgencyBusinessPO::getRelationId).collect(Collectors.toList());

        int expectPage = ywAgencyBusinessDTO.getP();
        int queryStartRowNumber = (ywAgencyBusinessDTO.getP() - 1) * ywAgencyBusinessDTO.getSize();

        List<YyContract> yyContracts = this.yyContractMapper.selectByPage(contractIdList, queryStartRowNumber , ywAgencyBusinessDTO.getSize());

        if(yyContracts.isEmpty()){
           return null;
        }

        //剔除重复的yyContract ，因为可能存在一个合同多个物业地址
        Map<Long , YyContract> yyContractIdMap = new HashMap<>();
        for (YyContract yyContract : yyContracts) {
            YyContract temp = yyContractIdMap.get(yyContract.getId());
            if(null != temp){
                String zlRentalArea = temp.getZlRentalArea();
                String zlMonthlyRent = temp.getZlMonthlyRent();

                String zlRentalArea1 = yyContract.getZlRentalArea();
                String zlMonthlyRent1 = yyContract.getZlMonthlyRent();

                BigDecimal newZlMonthlyRent = new BigDecimal(zlMonthlyRent).add(new BigDecimal(zlMonthlyRent1));
                BigDecimal newZlRentalArea = new BigDecimal(zlRentalArea).add(new BigDecimal(zlRentalArea1));

                yyContract.setZlMonthlyRent(newZlMonthlyRent.toString());
                yyContract.setZlRentalArea(newZlRentalArea.toString());
            }

            yyContractIdMap.put(yyContract.getId() , yyContract);
        }
        ArrayList<YyContract> newYyContractList = new ArrayList<>(yyContractIdMap.values());


        //计算总数
        int total = this.yyContractMapper.selectByPageCount(contractIdList);

        Map<Long , YwAgencyBusinessPO> map = new HashMap<>();
        for (YwAgencyBusinessPO ywAgencyBusinessPO : ywAgencyBusinessPOList) {
            map.put(ywAgencyBusinessPO.getRelationId() , ywAgencyBusinessPO);
        }

        List<MyContractVO> myContractVOList = new ArrayList<>();
        for (YyContract yyContract : newYyContractList) {
            YwAgencyBusinessPO tempBusiness = map.get(yyContract.getId());

            Date startRentDate = yyContract.getStartRentDate();
            Date endRentDate = yyContract.getEndRentDate();
            String startDateStr = DateUtil.formatDateTime(startRentDate);
            String endDateStr = DateUtil.formatDateTime(endRentDate);

            String zlMonthlyRent = addNumber(yyContract.getZlMonthlyRent());
            String zlRentalArea = addNumber(yyContract.getZlRentalArea());

            MyContractVO myContractVO = new MyContractVO();
            myContractVO.setId(yyContract.getId());
            myContractVO.setWyName(tempBusiness.getWyName());
            myContractVO.setLeaseDuringTime(startDateStr +"至" + endDateStr);
            myContractVO.setApplyPeopleType(yyContract.getApplicationType() == 1 ? "法人申请" : "委托人申请");
            myContractVO.setIsEconomicCommitment(yyContract.getIsEconomicCommit().toString());
            myContractVO.setMonthlyRent(zlMonthlyRent);
            myContractVO.setRentalArea(zlRentalArea);
            myContractVO.setSignType(tempBusiness.getWebSignType());
            myContractVO.setStartRentDate(startDateStr);
            myContractVO.setEndRentDate(endDateStr);
            myContractVO.setBusinessId(tempBusiness.getId());

            myContractVOList.add(myContractVO);
        }

        PageVO pageVO = new PageVO();
        pageVO.setList(myContractVOList);
        pageVO.setTotal(total);
        pageVO.setP(expectPage);
        pageVO.setSize(ywAgencyBusinessDTO.getSize());

        return pageVO;
    }

    private String addNumber(String zlRentalArea) {
        if (StringUtils.isNotEmpty(zlRentalArea)) {
            String[] rentRentArr = zlRentalArea.split(",");
            BigDecimal temp = new BigDecimal(0);
            for (String s : rentRentArr) {
                temp = temp.add(new BigDecimal(s));
            }
            zlRentalArea = temp.toString();
        }
        return zlRentalArea;
    }

    @Override
    public PageVO<MyApplyRecordVO> selectApplyList(YwAgencyBusinessDTO ywAgencyBusinessDTO) {
        String socialUniformCreditCode = ywAgencyBusinessDTO.getSocialUniformCreditCode();

        int expectPage = ywAgencyBusinessDTO.getP();
        int queryStartRowNumber = (ywAgencyBusinessDTO.getP() - 1) * ywAgencyBusinessDTO.getSize();

        QueryWrapper<YwAgencyBusinessPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("social_uniform_credit_code" , socialUniformCreditCode);
        queryWrapper.eq("record_status" , CommonEnum.PROCESS_APPLY);
        queryWrapper.last("limit " +queryStartRowNumber + ","+ywAgencyBusinessDTO.getSize());
        List<YwAgencyBusinessPO> ywAgencyBusinessPOS = this.ywAgencyBusinessMapper.selectList(queryWrapper);

        if(ywAgencyBusinessPOS.isEmpty()){
            return null;
        }

        QueryWrapper<YwAgencyBusinessPO> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("social_uniform_credit_code" , socialUniformCreditCode);
        queryWrapper2.eq("record_status" , CommonEnum.PROCESS_APPLY);
        Integer total = this.ywAgencyBusinessMapper.selectCount(queryWrapper2);

        List<MyApplyRecordVO> returnList = new ArrayList<>();
        for (YwAgencyBusinessPO ywAgencyBusinessPO : ywAgencyBusinessPOS) {
            MyApplyRecordVO myApplyRecordVO = new MyApplyRecordVO();
            myApplyRecordVO.setId(ywAgencyBusinessPO.getId());
            myApplyRecordVO.setApplicationTime(String.valueOf(ywAgencyBusinessPO.getApplicationTime()));
            myApplyRecordVO.setProName(ywAgencyBusinessPO.getProName());
            myApplyRecordVO.setLease(ywAgencyBusinessPO.getApplicantUnit());
            myApplyRecordVO.setBusinessType(ywAgencyBusinessPO.getBusinessType() + "-" + (null == ywAgencyBusinessPO.getApplyPeopleType() || ywAgencyBusinessPO.getApplyPeopleType() == 1 ? "法人申请" :"委托代理人申请"));
            myApplyRecordVO.setProcess(ywAgencyBusinessPO.getProcess());
            returnList.add(myApplyRecordVO);
        }

        PageVO pageVO = new PageVO();
        pageVO.setList(returnList);
        pageVO.setTotal(total);
        pageVO.setP(expectPage);
        pageVO.setSize(ywAgencyBusinessDTO.getSize());

        return pageVO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public YwAgencyBusinessPO apply(YwAgencyBusinessDTO ywAgencyBusinessDTO) {

        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessMapper.selectOneIdByBusinessId(String.valueOf(ywAgencyBusinessDTO.getId()));
        //受理号=HT+（签订、续签）首字母+NS+年+月+日+000（三位数编号）
        String webSignType =  "";
        String proName = ywAgencyBusinessPO.getApplicantUnit();
        boolean newSignflag = true;

        if("1".equals(ywAgencyBusinessDTO.getWebSignType())){
            webSignType = "QD";
            proName += "租赁合同新签申请";
        }else{
            webSignType = "XQ";
            proName += "租赁合同续签申请";
            newSignflag = false;
        }


        StringBuffer acceptCode = new StringBuffer();
        acceptCode.append("HT");
        acceptCode.append(webSignType);
        acceptCode.append(toolUtils.getPropertyCode());

        /**
         * 数据更新
         */
        ywAgencyBusinessPO.setApplyPeopleType(Byte.parseByte(String.valueOf(ywAgencyBusinessDTO.getYyContractVO().getApplicationType())));
        ywAgencyBusinessPO.setContactName(ywAgencyBusinessDTO.getYyContractVO().getContactPerson());
        ywAgencyBusinessPO.setContactPhone(ywAgencyBusinessDTO.getYyContractVO().getContactPhone());
        ywAgencyBusinessPO.setAgencyType(ywAgencyBusinessPO.getAgencyType());
        ywAgencyBusinessPO.setRecordStatus(CommonEnum.PROCESS_APPLY);
        ywAgencyBusinessPO.setAcceptCode(acceptCode.toString());
        ywAgencyBusinessPO.setProName(proName);
        ywAgencyBusinessPO.setSignSource(CommonEnum.SIGN_SOURCE_ONLINE_APPLY);
        ywAgencyBusinessPO.setApplicationTime(new Timestamp(System.currentTimeMillis()));
        ywAgencyBusinessPO.setRemark(ywAgencyBusinessDTO.getYyContractVO().getRemark());

        boolean flag = this.activitiBaseService.selectByBusinessId(String.valueOf(ywAgencyBusinessPO.getId()));
        if(flag){
            //情况1 存在实例 挂起时需要激活  情况2 重新申请时 需流转到下一节点
            this.activitiBaseService.activateProcessInstance(ywAgencyBusinessPO);
        }else{
            //新增 工作流启动
            this.activitiBaseService.addAgencyBusinessRecord(ywAgencyBusinessPO);
        }

        this.ywAgencyBusinessMapper.updateById(ywAgencyBusinessPO);

        //新增合同
        YyContract yyContract = new YyContract();;

        YyContractVO yyContractVO = ywAgencyBusinessDTO.getYyContractVO();
        BeanUtils.copyProperties(yyContractVO , yyContract);

        if(null != ywAgencyBusinessPO.getYyContractVO() && null != ywAgencyBusinessPO.getYyContractVO().getId()){
            yyContract.setId(ywAgencyBusinessPO.getYyContractVO().getId());
        }

        //设置合同模板类型
        //申请业务类型 1产业用房租赁 2商业用房租赁 3住宅用房租赁 4南山软件园租赁
        String businessType = ywAgencyBusinessDTO.getBusinessType();
        Integer isEconomicCommit = yyContract.getIsEconomicCommit();
        //类型 1产业用房租赁合同3+2（有经济贡献率）;2产业用房租赁合同3+2（无经济贡献率）;3产业用房租赁合同3年及3年以下; 4商业租赁合同；5住宅租赁合同；6南山软件租赁合同
        if(!StringUtils.isEmpty(businessType)){
            switch (businessType){
                case CommonEnum.BUSINESS_TYPE_1:
                    //未设置 产业用房租赁合同3年及3年以下
                    if(isEconomicCommit.equals(CommonEnum.IS_ECONOMIC_COMMIT_1)){
                        yyContract.setType(YyContractTypeEnum.CONTRACT_TYPE_1.getTypeCode());
                    }else if(isEconomicCommit.equals(CommonEnum.IS_ECONOMIC_COMMIT_2)){
                        yyContract.setType(YyContractTypeEnum.CONTRACT_TYPE_2.getTypeCode());
                    }
                    break;
                case CommonEnum.BUSINESS_TYPE_2:
                    yyContract.setType(YyContractTypeEnum.CONTRACT_TYPE_4.getTypeCode());
                    break;
                case CommonEnum.BUSINESS_TYPE_3:
                    yyContract.setType(YyContractTypeEnum.CONTRACT_TYPE_5.getTypeCode());
                    break;
                case CommonEnum.BUSINESS_TYPE_4:
                    yyContract.setType(YyContractTypeEnum.CONTRACT_TYPE_6.getTypeCode());
                    break;
                default:
                    break;
            }
        }


        yyContract.setContractType(Integer.parseInt(ywAgencyBusinessPO.getWebSignType()));
        yyContract.setLeaser(ywAgencyBusinessPO.getApplicantUnit());
        //固定为1 社会统一信用代码
        yyContract.setLeaserType(1);
        yyContract.setLeaserCode(ywAgencyBusinessPO.getSocialUniformCreditCode());
        yyContract.setLeaserPhone(ywAgencyBusinessPO.getContactPhone());
        yyContract.setContactPerson(ywAgencyBusinessPO.getContactName());
        yyContract.setContactPhone(ywAgencyBusinessPO.getContactPhone());

        // 附件id设置
        QueryWrapper<YwHandoverDetailsPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("yw_agency_business_id" , ywAgencyBusinessPO.getId());
        List<YwHandoverDetailsPO> ywHandoverDetailsPOList = this.ywHandoverDetailsMapper.selectList(queryWrapper);

        Map<String , YwHandoverDetailsPO> map = new HashMap<>();
        for (YwHandoverDetailsPO ywHandoverDetailsPO : ywHandoverDetailsPOList) {
            map.put(ywHandoverDetailsPO.getName() , ywHandoverDetailsPO);
        }

        String file1 = ContractFileNameEnum.File_1.getFileName();
        String file2 = ContractFileNameEnum.File_2.getFileName();
        String file3 = ContractFileNameEnum.File_3.getFileName();
        String file4 = ContractFileNameEnum.File_4.getFileName();
        String file5 = ContractFileNameEnum.File_5.getFileName();
        String file6 = ContractFileNameEnum.File_6.getFileName();
        String file7 = ContractFileNameEnum.File_7.getFileName();
        String file17 = ContractFileNameEnum.File_17.getFileName();

        /**
         * 入驻通知书
         * 营业执照复印件
         * 法人身份证复印件
         * 股东证明书
         * 法人代表证明书
         * 法人授权委托书
         * 经办人身份证复印件
         */
        YwHandoverDetailsPO ywHandoverDetailsPO2 = map.get(file2);
        YwHandoverDetailsPO ywHandoverDetailsPO3 = map.get(file3);
        YwHandoverDetailsPO ywHandoverDetailsPO4 = map.get(file5);
        YwHandoverDetailsPO ywHandoverDetailsPO5 = map.get(file4);
        YwHandoverDetailsPO ywHandoverDetailsPO6 = map.get(file6);
        YwHandoverDetailsPO ywHandoverDetailsPO7 = map.get(file7);
        yyContract.setBusinessLicenseFilePathId(null != ywHandoverDetailsPO2 ? ywHandoverDetailsPO2.getHandoverDetailsImgId() : null);
        yyContract.setLegalPersonFilePathId(null != ywHandoverDetailsPO3? ywHandoverDetailsPO3.getHandoverDetailsImgId() : null);
        yyContract.setLegalRepresentativeFilePathId(null != ywHandoverDetailsPO4 ? ywHandoverDetailsPO4.getHandoverDetailsImgId() : null);
        yyContract.setShareholderFilePathId(null != ywHandoverDetailsPO5 ? ywHandoverDetailsPO5.getHandoverDetailsImgId() : null);
        yyContract.setLegalPersonPowerFilePathId(null != ywHandoverDetailsPO6 ? ywHandoverDetailsPO6.getHandoverDetailsImgId() : null);
        yyContract.setHandlingPersonCardFilePathId(null != ywHandoverDetailsPO7 ? ywHandoverDetailsPO7.getHandoverDetailsImgId() : null);

        if(newSignflag){
            YwHandoverDetailsPO ywHandoverDetailsPO1 = map.get(file1);
            yyContract.setResidentNoticeFilePathId(null != ywHandoverDetailsPO1 ? ywHandoverDetailsPO1.getHandoverDetailsImgId() : null);
        }else{
            YwHandoverDetailsPO ywHandoverDetailsPO8 = map.get(file17);
            yyContract.setRenewalSignFilePathId(null != ywHandoverDetailsPO8 ? ywHandoverDetailsPO8.getHandoverDetailsImgId() : null);
        }

        Timestamp nowTime = new Timestamp(System.currentTimeMillis());

        if(flag){
            yyContract.setUpdateTime(nowTime);
            this.yyContractMapper.updateById(yyContract);
        }else{
            yyContract.setId(IdWorker.getId());
            yyContract.setTableId(yyContract.getId());
            yyContract.setCreateTime(nowTime);
            this.yyContractMapper.insert(yyContract);
        }

        ywAgencyBusinessPO.setRelationId(yyContract.getId());
        this.ywAgencyBusinessMapper.updateById(ywAgencyBusinessPO);

        if(!ywHandoverDetailsPOList.isEmpty()){
            for (YwHandoverDetailsPO ywHandoverDetailsPO : ywHandoverDetailsPOList) {
                ywHandoverDetailsPO.setYyContractId(yyContract.getId());
                this.ywHandoverDetailsMapper.updateById(ywHandoverDetailsPO);
            }
        }

        YwApplyChangePO ywApplyChangePO = new YwApplyChangePO();
        ywApplyChangePO.setOperatingTime(String.valueOf(System.currentTimeMillis()));
        ywApplyChangePO.setOperation("提交");
        ywApplyChangePO.setYwAgencyBusinessId(ywAgencyBusinessDTO.getId());
        this.ywApplyChangeMapper.insert(ywApplyChangePO);

        //更新合同物业关联表
        YyContractWyRelationPO yyContractWyRelationPO = new YyContractWyRelationPO();
        yyContractWyRelationPO.setYyContractId(yyContract.getId());
        yyContractWyRelationPO.setYwBusinessId(ywAgencyBusinessDTO.getId());

        this.yyContractWyRelationMapper.updateByBusinessId(yyContractWyRelationPO);


        return ywAgencyBusinessPO;

    }

    @Override
    public int deleteFile(String fileId) {
        int i = 0;
        SysFilePath sysFilePath = this.sysFilePathMapper.selectById(fileId);
        if(null != sysFilePath){
            String path = sysFilePath.getPath();
            String preFix = createFileUtil.isWindows() ? winUrl : linuxUrl;

            FileUtils.deleteFile(preFix + "/" + path);
            i = this.sysFilePathMapper.deleteById(sysFilePath.getId());

            QueryWrapper<YwHandoverDetailsPO> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("handover_details_img_id" , fileId);
            YwHandoverDetailsPO ywHandoverDetailsPO = this.ywHandoverDetailsMapper.selectOne(queryWrapper);

            ywHandoverDetailsPO.setHandoverDetailsImgId(null);
            this.ywHandoverDetailsMapper.updateByPrimaryKey(ywHandoverDetailsPO);
        }

        return i;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean applyUndo(YwAgencyBusinessDTO ywAgencyBusinessDTO) {
        long businessId = ywAgencyBusinessDTO.getId();
        if (businessId < 1){
            return false;
        }

        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();
        ywAgencyBusinessPO.setId(ywAgencyBusinessDTO.getId());
        ywAgencyBusinessPO.setRecordStatus(CommonEnum.PROCESS_APPLY_UNDO);
        this.ywAgencyBusinessMapper.updateById(ywAgencyBusinessPO);

        YwApplyChangePO ywApplyChangePO = new YwApplyChangePO();
        ywApplyChangePO.setOperatingTime(String.valueOf(System.currentTimeMillis()));
        ywApplyChangePO.setOperation("撤销");
        ywApplyChangePO.setYwAgencyBusinessId(ywAgencyBusinessDTO.getId());
        this.ywApplyChangeMapper.insert(ywApplyChangePO);

        return this.activitiBaseService.suspendProcessInstance(String.valueOf(businessId));
    }

    @Override
    public List<String> selectUndoApply(YwAgencyBusinessDTO ywAgencyBusinessDTO) {

        int p = ywAgencyBusinessDTO.getP();
        int size = ywAgencyBusinessDTO.getSize();

        int queryNumber = (p - 1) * size;

        QueryWrapper<YwAgencyBusinessPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNull("process");
        queryWrapper.select("id");
        queryWrapper.last("limit " + queryNumber + "," + size);
        List<YwAgencyBusinessPO> ywAgencyBusinessPOS = this.ywAgencyBusinessMapper.selectList(queryWrapper);

        List<String> returnList = new ArrayList<>();
        if(null != ywAgencyBusinessPOS && !ywAgencyBusinessPOS.isEmpty()){
            for (YwAgencyBusinessPO ywAgencyBusinessPO : ywAgencyBusinessPOS) {
                returnList.add(String.valueOf(ywAgencyBusinessPO.getId()));
            }
        }

        return returnList;
    }

    @Override
    public Map<String, Integer> countNumber(YwAgencyBusinessDTO ywAgencyBusinessDTO) {

        if(StringUtils.isEmpty(ywAgencyBusinessDTO.getSocialUniformCreditCode())){
            throw new BusinessException(ErrorCodeEnum.SOCIAL_UNITFORM_CREDIT_CODE_ERROR);
        }

        Map<String , Integer> returnMap = new HashMap<>();
        //我的收藏
        Integer collectTotal = this.wyCollectionMapper.selectWyIdBySocialCodeTotal(ywAgencyBusinessDTO.getSocialUniformCreditCode());
        returnMap.put("myFavoriteNumber" ,collectTotal);

        /**
         * 统计我的合同数量
         * 统计我的入驻
         */
        int myContractNumber = 0;
        Integer myHouseNumber = 0;
        List<YwAgencyBusinessPO> ywAgencyBusinessPOList = this.activitiBaseService.selectProcessCompleteList(ywAgencyBusinessDTO.getSocialUniformCreditCode());
        if(null != ywAgencyBusinessPOList && !ywAgencyBusinessPOList.isEmpty()){

            List<Long> contractIdList = ywAgencyBusinessPOList.stream().map(YwAgencyBusinessPO::getRelationId).collect(Collectors.toList());
            myContractNumber = this.yyContractMapper.selectByPageCount(contractIdList);

            QueryWrapper<YyContractWyRelationPO> yyContractWyRelationPOQueryWrapper = new QueryWrapper<>();
            yyContractWyRelationPOQueryWrapper.in("yy_contract_id" , contractIdList);
            List<YyContractWyRelationPO> yyContractWyRelationPOS = this.yyContractWyRelationMapper.selectList(yyContractWyRelationPOQueryWrapper);
            if(null != yyContractWyRelationPOS && !yyContractWyRelationPOS.isEmpty()){
                List<Long> collect = yyContractWyRelationPOS.stream().map(YyContractWyRelationPO::getWyBasicInfoId).collect(Collectors.toList());

                QueryWrapper<WyBasicInfo> queryWrapper = new QueryWrapper<>();
                queryWrapper.in("id" , collect);
                myHouseNumber = this.wyBasicInfoMapper.selectCount(queryWrapper);
            }
        }
        returnMap.put("myContractNumber" , myContractNumber);
        returnMap.put("myHouseNumber" , myHouseNumber);
        /**
         * 统计我的申请记录数量
         */
        QueryWrapper<YwAgencyBusinessPO> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("social_uniform_credit_code" , ywAgencyBusinessDTO.getSocialUniformCreditCode());
        queryWrapper2.eq("record_status" , CommonEnum.PROCESS_APPLY);
        Integer myApplyNumber = this.ywAgencyBusinessMapper.selectCount(queryWrapper2);
        returnMap.put("myApplyNumber" , myApplyNumber);

        return returnMap;
    }

    @Override
    public boolean canUndo(YwAgencyBusinessDTO ywAgencyBusinessDTO) {

        String secondActivityId = this.activitiBaseService.getSecondActivityId(ywAgencyBusinessDTO);
        Execution execution = this.runtimeService.createExecutionQuery().processInstanceBusinessKey(String.valueOf(ywAgencyBusinessDTO.getId())).singleResult();
        if (null != execution) {
            return secondActivityId.equals(execution.getActivityId());
        }

        return false;
    }

}
