package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.DevSpaceHandoverDTO;
import com.zbkc.model.dto.GetHomeListDTO;
import com.zbkc.model.po.YwDevHandOver;
import com.zbkc.mapper.YwDevHandOverMapper;
import com.zbkc.model.vo.DevSpaceHandoverProVO;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.YwDevHandOverService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Wrapper;
import java.util.List;

/**
 * <p>
 * 移交申请-开发商空间移交项目表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
@Service
public class YwDevHandOverServiceImpl extends ServiceImpl<YwDevHandOverMapper, YwDevHandOver> implements YwDevHandOverService {
    @Autowired
    private YwDevHandOverMapper ywDevHandOverMapper;
    @Autowired
    private ToolUtils toolUtils;

    @Override
    public ResponseVO addDevSpaceHandoverPro(YwDevHandOver dto) {
        dto.setIsDel(1);
        dto.setId(IdWorker.getId());
        dto.setProCode(toolUtils.projectCodeGeneratorYJYF());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, ywDevHandOverMapper.insert(dto));
    }

    @Override
    public ResponseVO updDevSpaceHandoverPro(DevSpaceHandoverDTO dto) {
        YwDevHandOver ywDevHandOver = new YwDevHandOver();
        ywDevHandOver.setIsDel(1);
        BeanUtils.copyProperties(dto,ywDevHandOver);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,ywDevHandOverMapper.updateById(ywDevHandOver));
    }

    @Override
    public ResponseVO details(Long id) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,ywDevHandOverMapper.selectById(id));
    }

    @Override
    public ResponseVO devSpaceHandoverProList(GetHomeListDTO dto) {
        if (dto.getP() <= 0 || dto.getSize() < 0) {
            throw new BusinessException(ErrorCodeEnum.PAGE_NUMBER_ERROR);
        }
        dto.setP((dto.getP() - 1) * dto.getSize());
        List<DevSpaceHandoverProVO> list = ywDevHandOverMapper.devSpaceHandoverProList(dto);
        System.out.println(list);
        Integer total = ywDevHandOverMapper.countDevSpaceHandoverProList(dto);
        PageVO<DevSpaceHandoverProVO> vo = new PageVO<>();
        vo.setList(list);
        vo.setSize(dto.getSize());
        vo.setP(dto.getP() / dto.getSize() + 1);
        vo.setTotal(total);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, vo);
    }

    @Override
    public ResponseVO delDevSpaceHandoverPro(Long id) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,ywDevHandOverMapper.delDevSpaceHandoverPro(id));
    }


}
