package com.zbkc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.po.WyCollectionPO;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/11/25
 */
public interface WyCollectionService extends IService<WyCollectionPO> {

}
