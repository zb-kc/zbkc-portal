package com.zbkc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.po.YwHandoverDetailsPO;
import com.zbkc.model.vo.ResponseVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 业务办理——移交用房详情 服务类
 * @author ZB3436.xiongshibao
 * @date 2021/10/12
 */
public interface YwHandoverDetailsSerivce extends IService<YwHandoverDetailsPO> {

    /**
     * 根据业务id 查询
     * @param businessId 业务id
     * @return List<YwHandoverDetailsPO>
     */
    List<YwHandoverDetailsPO> selectList(String businessId);

    /**
     * 材料通过
     * @param handoverDetailsId 附件材料审核状态表id
     * @return 表id
     */
    int dataPass(String handoverDetailsId);

    /**
     * 根据id 下载对应的附件
     * @param ywHandlerDetailsId 附件材料审核状态表id
     */
    void downloadFileOne(HttpServletResponse response, String ywHandlerDetailsId);

    /**
     * 校验图片关联id是否正确
     * @param imgId 图片关联id
     * @return ResponseVO
     */
    ResponseVO checkImgId(String imgId);
}
