package com.zbkc.service;

import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.po.YwAgencyBusinessPO;

import java.util.List;

/**
 * 工作流服务类
 * @author ZB3436.xiongshibao
 * @since 2021-9-16
 */
public interface ActivitiBaseService {

    /**
     * 根据用户id查询已经完成的待办信息
     * @param userId 用户id
     * @return List<YwAgencyBusinessPO>
     */
    List<YwAgencyBusinessPO> selectProcessCompleteList(String userId);

    /**
     * 根据业务id 创建流程实例
     * @param ywAgencyBusinessPO 移交用房申请DTO
     */
    void startProcessInstance(YwAgencyBusinessPO ywAgencyBusinessPO);

    /**
     * 新增业务办理-待办业务表，并开启流程
     * @param ywAgencyBusinessPO ywAgencyBusinessPO
     */
    void addAgencyBusinessRecord(YwAgencyBusinessPO ywAgencyBusinessPO);

    /**
     * 审核通过-完成节点
     * @param businessId 业务id
     * @param opinion 审核意见
     */
    void completeTask(String businessId , String opinion);

    /**
     * 根据业务id 查询流程状态
     * @param businessId 业务id
     * @return 流程状态
     */
    String getProcessStatus(String businessId);

    /**
     * 判断业务id流程是否存在
     * @param businessId 业务id
     * @return true 存在 false 不存在
     */
    boolean selectByBusinessId(String businessId);

    /**
     * 挂起流程实例--用做申请的撤销
     * @param businessId 业务id
     */
    boolean suspendProcessInstance(String businessId);

    /**
     * 激活流程实例--用做重新申请
     * @param ywAgencyBusinessPO 业务
     * @return boolean
     */
    boolean activateProcessInstance(YwAgencyBusinessPO ywAgencyBusinessPO);

    /**
     * 根据业务id 查询流程中第二个activityId
     * @param ywAgencyBusinessDTO dto
     * @return 第二个activityId
     */
    String getSecondActivityId(YwAgencyBusinessDTO ywAgencyBusinessDTO);
}
