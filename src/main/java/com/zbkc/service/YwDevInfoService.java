package com.zbkc.service;

import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.po.YwDevInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.vo.FileVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * （新）业务办理——开发商基本信息表（移交用房申请） 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
public interface YwDevInfoService extends IService<YwDevInfo> {

    /**
     * 保存文件
     * @param vo FileVo
     * @param ywHandlerDetailsId ywHandlerDetailsId
     * @return 文件
     */
    SysFilePath1 saveSysFilePath(FileVo vo, String ywHandlerDetailsId);

    /**
     * 下载单个文件
     * @param response response
     * @param ywHandlerDetailsId 附件材料表id
     */
    void downloadFileOne(HttpServletResponse response, String ywHandlerDetailsId);

    /**
     * 删除附件
     * @param fileId 附件id
     * @return 影响行数
     */
    int deleteFile(String fileId);

    /**
     * 打包下载附件
     * @param response response
     * @param downloadFileIds 多个附件材料表id
     */
    void downloadBatchByFile(HttpServletResponse response, String downloadFileIds);

    /**
     * 下载导入模板
     * @param response response
     * @param templateFileType 模板类型
     */
    void downloadFileTemplate(HttpServletResponse response, String templateFileType);

    /**
     * 读取上传的用房详情模板 导入数据库
     * @param file 用房详情信息
     * @param ywDevInfoId
     * @return
     */
    int uploadFileTemplate(MultipartFile file, Long ywDevInfoId);
}
