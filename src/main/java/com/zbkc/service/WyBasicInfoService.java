package com.zbkc.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.dto.GoverDTO;
import com.zbkc.model.dto.QueryWyInfoDTO;
import com.zbkc.model.dto.WyCollectionDTO;
import com.zbkc.model.po.WyBasicInfo;
import com.zbkc.model.vo.ResponseVO;

/**
 * <p>
 * 物业基础信息表	 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-10-20
 */
public interface WyBasicInfoService extends IService<WyBasicInfo> {

    ResponseVO homeList(String socialUniformCreditCode);

    ResponseVO myEnter(String socialUniformCreditCode);

    ResponseVO getDetails(Long id);

    ResponseVO goverWy(GoverDTO dto);

    /**
     * 查看更多 物业列表
     * @param queryWyInfoDTO 筛选条件
     * @return vo
     */
    ResponseVO showMore(QueryWyInfoDTO queryWyInfoDTO);

    /**
     * 收藏物业
     * @param wyCollectionDTO dto
     * @return vo
     */
    ResponseVO collect(WyCollectionDTO wyCollectionDTO);

    /**
     * 查看单个物业信息
     * @param id 物业id
     * @param socialUniformCreditCode 统一社会信用代码
     * @return vo
     */
    ResponseVO selectOneWyInfo(Long id, String socialUniformCreditCode);

    /**
     * 查询我的收藏
     * @param queryWyInfoDTO dto
     * @return vo
     */
    ResponseVO selectMyCollection(QueryWyInfoDTO queryWyInfoDTO);
}
