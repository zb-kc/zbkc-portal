package com.zbkc.service;

import com.zbkc.model.po.YwDevBuildingUsedPO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 移交用房-用房详情表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
public interface YwDevBuildingUsedService extends IService<YwDevBuildingUsedPO> {

}
