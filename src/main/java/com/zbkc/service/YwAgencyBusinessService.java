package com.zbkc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.po.YwApplyChangePO;
import com.zbkc.model.vo.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/9/27
 */
public interface YwAgencyBusinessService extends IService<YwAgencyBusinessPO> {

    /**
     * 增加
     * @param ywAgencyBusinessPO 移交用房申请po
     * @return id
     */
    int addYwAgencyBusiness(YwAgencyBusinessPO ywAgencyBusinessPO);
    /**
     * 删除
     * @param ywAgencyBusinessPO 移交用房申请po
     * @return id
     */
    int removeYwAgencyBusiness(YwAgencyBusinessPO ywAgencyBusinessPO);
    /**
     * 更新
     * @param ywAgencyApplicationPO 移交用房申请po
     * @return id
     */
    int updateYwAgencyBusiness(YwAgencyBusinessDTO ywAgencyApplicationPO);
    /**
     * 查询根据id
     * @param ywAgencyBusinessDTO 移交用房申请po
     * @return id
     */
    YwAgencyBusinessPO selectYwAgencyBusinessById(YwAgencyBusinessDTO ywAgencyBusinessDTO);
    /**
     * 分页查询
     * @param ywAgencyBusinessDTO 移交用房申请po
     * @return ResponseVO
     */
    PageVO<YwAgencyBusinessVO> pageYwAgencyBusinessList(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    /**
     * 查询一条申请记录
     * @param businessId 业务id
     * @return  申请对象
     */
    YwAgencyBusinessDTO selectOne(String businessId);

    /**
     * 保存文件路径到数据库
     * @param vo 文件vo
     * @param ywHandlerDetailsId 申请材料id
     * @return  文件路径
     */
    SysFilePath1 saveSysFilePath(FileVo vo, String ywHandlerDetailsId);

    /**
     * 打包下载文件
     * @param response HttpServletResponse
     * @param downloadFileIds 要下载的文件id
     */
    void downloadBatchByFile(HttpServletResponse response, String downloadFileIds);

    /**
     * 根据业务id 查询合同信息
     * @param businessId 业务id
     * @return YwContractDTO
     */
    YwContractVO showOldContract(String businessId);

    /**
     * 新增申请调整记录
     * @param ywApplyChangePO po
     */
    int addApplyRecord(YwApplyChangePO ywApplyChangePO);

    /**
     * 根据用户id查询待办业务表
     * @param ywAgencyBusinessDTO dto
     */
    PageVO<MyContractVO> selectBySocialUniformCreditCode(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    /**
     * 根据用户id查询申请记录
     * @param ywAgencyBusinessDTO dto
     * @return list
     */
    PageVO<MyApplyRecordVO> selectApplyList(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    /**
     * 申请
     * @param ywAgencyBusinessDTO dto
     */
    YwAgencyBusinessPO apply(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    /**
     * 删除文件
     * @param fileId 文件id
     * @return 影响行数
     */
    int deleteFile(String fileId);

    /**
     * 撤销申请
     * @param ywAgencyBusinessDTO dto
     * @return boolean
     */
    boolean applyUndo(YwAgencyBusinessDTO ywAgencyBusinessDTO);


    /**
     * 查询撤销的申请记录
     * @param ywAgencyBusinessDTO dto
     * @return list
     */
    List<String> selectUndoApply(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    /**
     * 计算我的入驻 我的收藏 我的合同 申请记录
     * @param ywAgencyBusinessDTO dto
     * @return map
     */
    Map<String , Integer> countNumber(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    /**
     * 检测是否是够撤回
     * @param ywAgencyBusinessDTO dto
     * @return true 能 false  否
     */
    boolean canUndo(YwAgencyBusinessDTO ywAgencyBusinessDTO);
}
