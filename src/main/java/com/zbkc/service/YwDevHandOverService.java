package com.zbkc.service;

import com.zbkc.model.dto.DevSpaceHandoverDTO;
import com.zbkc.model.dto.GetHomeListDTO;
import com.zbkc.model.po.YwDevHandOver;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.vo.ResponseVO;

/**
 * <p>
 * 移交申请-开发商空间移交项目表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
public interface YwDevHandOverService extends IService<YwDevHandOver> {

    ResponseVO addDevSpaceHandoverPro(YwDevHandOver dto);

    ResponseVO updDevSpaceHandoverPro(DevSpaceHandoverDTO dto);

    ResponseVO details(Long id);

    ResponseVO devSpaceHandoverProList(GetHomeListDTO dto);

    ResponseVO delDevSpaceHandoverPro(Long id);
}
