package com.zbkc;

import org.activiti.spring.boot.SecurityAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *  @author gmding
 *  @date 2021-7-16
 * */
@EnableSwagger2
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class,org.activiti.spring.boot.SecurityAutoConfiguration.class,
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@EnableScheduling       //开启定时器
public  class ZbkcPortalApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZbkcPortalApplication.class, args);
    }

}
