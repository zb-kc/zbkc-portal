package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.YyContract;
import com.zbkc.model.vo.YwContractVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 运营管理——合同管理 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-13
 */
@Repository
public interface YyContractMapper extends BaseMapper<YyContract> {


   @Select("select * from  yy_contract where id=#{id}")
   YyContract selectById(@Param("id") Long id);

   /**
    * 业务办理中 合同查询
    * @param bussinessId 业务id
    * @return 合同信息
    */
   YwContractVO selectYwContractIdByBusinessId(String bussinessId);

   List<YyContract> selectByPage(@Param("contractIdList")List<Long> contractIdList,@Param("queryStartRowNumber") int queryStartRowNumber,@Param("size") int size);

   int selectByPageCount(@Param("contractIdList")List<Long> contractIdList);
}
