package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.YwHandoverDetailsPO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/12
 */
@Repository
public interface YwHandoverDetailsMapper extends BaseMapper<YwHandoverDetailsPO> {

    int deleteByPrimaryKey(Long id);

    int insertSelective(YwHandoverDetailsPO record);

    List<YwHandoverDetailsPO> selectByBusinessId(Long id);

    int updateByPrimaryKeySelective(YwHandoverDetailsPO record);

    int updateByPrimaryKey(YwHandoverDetailsPO record);
}
