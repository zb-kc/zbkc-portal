package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.SysDataType;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 数据字典类型表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-08-03
 */
@Repository
public interface SysDataTypeMapper extends BaseMapper<SysDataType> {


    /**
     * @param name 名称
     * @return   List<SysDataType>
     */
    @Select("select * from sys_data_type where name=#{name}")
    List<SysDataType> selectByName(@Param("name") String name);

    /**
     * @param id 主键id
     * @return  Integer
     */
    @Update("UPDATE sys_data_type SET status = 1 WHERE id = #{id}")
    Integer statusOpen(@Param("id") Long id);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE sys_data_type SET status = 2 WHERE id = #{id}")
    Integer statusClose(@Param("id") Long id);

    /**
     * @param p 页码
     * @param size 行数
     * @param name 名称
     * @return List<SysDataType>
     */
    @Select("select * from sys_data_type where name like '%' #{name} '%' or code like '%' #{name} '%' or remark like '%' #{name} '%' order by cast(create_time as datetime) DESC limit #{p},#{size}")
    List<SysDataType> pageByName(@Param("p") Integer p, @Param("size") Integer size, @Param("name") String name);


    /**
     * @param name 名称
     * @return  List<SysDataType>
     */
    @Select("select id from sys_data_type where name like '%' #{name} '%' or code like '%' #{name} '%' or remark like '%' #{name} '%'")
    List<SysDataType> selectByNameTotal(@Param("name") String name);

    /**
     * 根据code查所有物业
     * @return  List<SysDataType>
     */
    @Select("select * from sys_data_type  where code = #{code} order by cast(create_time as datetime) DESC")
    List<SysDataType> selectAllByCode(@Param("code") String code);


    /**
     * 根据code查所有物业-存货状态
     * @return  List<SysDataType>
     */
    @Select("select * from sys_data_type  where code = #{code} and status = 1 order by cast(create_time as datetime) DESC")
    List<SysDataType> selectAllByCodeLive(@Param("code") String code);

}
