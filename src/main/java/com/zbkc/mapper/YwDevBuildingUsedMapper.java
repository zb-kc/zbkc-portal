package com.zbkc.mapper;

import com.zbkc.model.po.YwDevBuildingUsedPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 移交用房-用房详情表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
@Repository
public interface YwDevBuildingUsedMapper extends BaseMapper<YwDevBuildingUsedPO> {

    /**
     * 批量插入数据
     * @param ywDevBuildingUsedList list
     * @return 影响行数
     */
    int insertBatch(@Param("list") List<YwDevBuildingUsedPO> ywDevBuildingUsedList);
}
