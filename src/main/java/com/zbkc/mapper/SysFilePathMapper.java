package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.SysFilePath;
import com.zbkc.model.po.SysFilePath1;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 *
 * @author gmding
 * @since 2021-08-18
 */
@Repository
public interface SysFilePathMapper extends BaseMapper<SysFilePath> {

    int addSysFilePath(SysFilePath sysFilePath);

    int addSysFilePath1(SysFilePath1 sysFilePath);

    List<SysFilePath1> getBydbFileId(@Param("dbFileId") Long dbFileId);

    int deleteBydbFileId(@Param("dbFileId") Long dbFileId);
}
