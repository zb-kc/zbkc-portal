package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.YwApplyChangePO;
import org.springframework.stereotype.Repository;

/**
 * @author ZB3436.xiongshibao
 * @date 2021-10-22
 */
@Repository
public interface YwApplyChangeMapper extends BaseMapper<YwApplyChangePO> {
}