package com.zbkc.mapper;

import com.zbkc.model.po.YwDevInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * （新）业务办理——开发商基本信息表（移交用房申请） Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
public interface YwDevInfoMapper extends BaseMapper<YwDevInfo> {

}
