package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.vo.YwAgencyBusinessVO;
import com.zbkc.model.vo.YwProcessInfoVO;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021-9-27
 */
@Repository
public interface YwAgencyBusinessMapper extends BaseMapper<YwAgencyBusinessPO> {

    /**
     * 查询待办业务-移交用房申请 分页查询
     * @param dto YwAgencyBusinessDTO数据传输对象
     * @return List<YwAgencyBusinessPO>
     */
    List<YwAgencyBusinessVO> selectYwAgencyBusinessByPage(YwAgencyBusinessDTO dto);

    int selectYwAgencyBusinessByPageCount(YwAgencyBusinessDTO dto);

    /**
     * 根据用户id查询 用户所属组织下的所有用户的申请信息
     * @param dto
     * @return
     */
    List<YwAgencyBusinessPO> selectYwAgencyBusinessByUserId(YwAgencyBusinessDTO dto);

    int selectYwAgencyBusinessByUserIdCount(YwAgencyBusinessDTO dto);

    /**
     * 根据业务id 查询业务流程各节点状态信息
     * @param businessKey 业务id
     * @return YwProcessInfoPO集合
     */
    List<YwProcessInfoVO> findProcessStatusByBusinessKey(@Param("businessKey") String businessKey);

    YwAgencyBusinessPO selectOneIdByBusinessId(String businessId);

}
