package com.zbkc.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.YyContractWyRelationPO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021-10-26
 */
@Repository
public interface YyContractWyRelationMapper extends BaseMapper<YyContractWyRelationPO> {

    @Select("select wy_basic_info_id from yy_contract_wy_relation where yy_contract_id =#{contractId}")
    List<Long> wyBasicIdsList(Long contractId);

    /**
     * 根据业务id 更新合同物业关联表
     * @param yyContractWyRelationPO po
     * @return 影响行数
     */
    int updateByBusinessId(YyContractWyRelationPO yyContractWyRelationPO);

    /**
     * 根据统一信息代码查询物业id
     * @param socialUniformCreditCode 统一信息代码
     * @return wyIdList
     */
    List<Long> selectWyIdBySocialCode(@Param("socialUniformCreditCode") String socialUniformCreditCode);
}