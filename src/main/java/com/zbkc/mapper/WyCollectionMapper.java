package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.WyCollectionPO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 */
@Repository
public interface WyCollectionMapper extends BaseMapper<WyCollectionPO> {

    /**
     * 根据社会统一信用代码查询 物业id --物业收藏表
     * @param socialUniformCreditCode 社会统一信用代码查询
     * @param queryNumber p
     * @param size size
     * @return list
     */
    List<Long> selectWyIdBySocialCode(@Param("socialUniformCreditCode") String socialUniformCreditCode, @Param("p") Integer queryNumber, @Param("size")Integer size);

    /**
     * 公司 收藏物业总数
     * @param socialUniformCreditCode 社会统一信用代码查询
     * @return total
     */
    Integer selectWyIdBySocialCodeTotal(@Param("socialUniformCreditCode") String socialUniformCreditCode);

    /**
     * 查询是否存在收藏信息
     * @param socialUniformCreditCode 社会统一信用代码查询
     * @param wyBasicInfoId 物业id
     * @return 影响行数
     */
    Integer selectByCodeAndWyId(@Param("socialUniformCreditCode") String socialUniformCreditCode, @Param("wyBasicInfoId") Long wyBasicInfoId);
}