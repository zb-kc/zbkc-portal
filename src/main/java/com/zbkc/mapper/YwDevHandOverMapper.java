package com.zbkc.mapper;

import com.zbkc.model.dto.GetHomeListDTO;
import com.zbkc.model.po.YwDevHandOver;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.vo.DevSpaceHandoverProVO;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 移交申请-开发商空间移交项目表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-12-23
 */
@Repository
public interface YwDevHandOverMapper extends BaseMapper<YwDevHandOver> {

    List<DevSpaceHandoverProVO> devSpaceHandoverProList (GetHomeListDTO dto);

    Integer countDevSpaceHandoverProList(GetHomeListDTO dto);

    @Update("update yw_dev_hand_over set is_del=2 where id =#{id}")
    Integer delDevSpaceHandoverPro(Long id);
}
