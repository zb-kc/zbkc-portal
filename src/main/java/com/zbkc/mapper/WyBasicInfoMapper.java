package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.dto.GoverDTO;
import com.zbkc.model.dto.QueryWyInfoDTO;
import com.zbkc.model.po.SysDataType;
import com.zbkc.model.po.SysUser;
import com.zbkc.model.po.WyBasicInfo;
import com.zbkc.model.po.WyFacilitiesInfo;
import com.zbkc.model.vo.WyBasicInfoHomeVo;
import com.zbkc.model.vo.WyBasicInfoVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 * 物业基础信息表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-10-20
 */
@Repository
public interface WyBasicInfoMapper extends BaseMapper<WyBasicInfo> {

    List<SysDataType> keyList();

    List<WyBasicInfoHomeVo> valueList(Long dataTypeId);

    Long searchId(String name);

//    List<WyBasicInfo> myEnter(Long userId);

    SysUser selectByid(Long userId);

    WyBasicInfo getDetailsList(Long id);

    List<WyFacilitiesInfo> selectFaciliesById(@Param( "wyBasicInfoId" )Long wyBasicInfoId);

    List<WyBasicInfo>  goverWy(GoverDTO dto);

    WyBasicInfo selectBywyId(Long wyId);

    /**
     * 统计入驻人数 根据物业id
     * @param wyId 物业id
     * @return 入驻人数
     */
    Integer countEnterNumber(@Param("wyId") Long wyId);

    /**
     * 查看更多 物业列表
     * @param queryWyInfoDTO dto
     * @return list
     */
    List<WyBasicInfoHomeVo> showMore(@Param("param") QueryWyInfoDTO queryWyInfoDTO);

    /**
     * 查看更多 物业列表 总数
     * @param queryWyInfoDTO dto
     * @return int
     */
    Integer showMoreTotal(@Param("param") QueryWyInfoDTO queryWyInfoDTO);

    List<WyBasicInfo>  goverWyTotal(GoverDTO dto);

    /**
     * 查询单个物业信息
     * @param id 物业id
     * @return info
     */
    WyBasicInfoVO selectOneWyInfo(@Param("wyId") Long id);


    /**
     * 门户网站首页列表查询 分组查询
     * @return list
     */
    List<WyBasicInfoHomeVo> selectWyBasicInfoHomeList();

    /**
     * 统计入驻人数 根据物业id
     * @param wyId 物业id
     * @return 入驻人数
     */
    Integer countEnterNumber2(@Param("wyId") Long wyId);
}
