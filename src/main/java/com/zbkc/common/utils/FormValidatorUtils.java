package com.zbkc.common.utils;

import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * 表单校验工具类
 *
 * @author ZB3436.xiongshibao
 * @date 2021/11/30
 */
@Component
public class FormValidatorUtils implements Serializable {

    private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

    /**
     * 校验参数
     * @param obj 表单参数
     */
    public void validate(Object obj) {

        if(null == obj){
            return;
        }

        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(obj);

        List<String> messageList = new ArrayList<>();
        if(null != constraintViolations && !constraintViolations.isEmpty()){
            for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
                messageList.add(constraintViolation.getMessage());
            }

            //存在 异常信息则 处理异常
            if(!messageList.isEmpty()){
                throw new BusinessException(ErrorCodeEnum.FORM_VALIDATOR_ERR.getCode() , Arrays.toString(messageList.toArray()));
            }
        }

    }

}
