package com.zbkc.common.utils;

import dm.jdbc.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tools.ant.util.DateUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 读取excel工具类
 *
 * @author ZB3436.xiongshibao
 * @date 2021/12/24
 */
@Slf4j
@Component
public class ReadExcelUtils {

    private static final String EXCEL_XLS = ".xls";
    private static final String EXCEL_XLSX = ".xlsx";

    /**
     * 用房详情表 中文对应属性名map
     */
    public static Map<String , String> YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP = new HashMap<>();

    {

        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("物业种类","wyType");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("期数","periodNumber");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("栋号","buildingNumber");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("层数","floorNumber");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("房号","roomNumber");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("具体用途","concretePurpose");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("楼栋结构形式","buildingStructuralStyle");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("移交方式","handOverMode");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("计划移交时间","handOverPlanTime");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("楼栋总高度","buildingTotalHeight");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("楼栋总层数","buildingTotalFloor");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("用房建筑面积","buildArea");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("本层层高","thisFloorHeight");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("本层可用电梯数量","thisFloorLiftNumber");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("电梯荷载","liftLoad");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("装修面净高","renovatingHeight");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("设计荷载","designLoad");
        YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.put("是否设置无障碍电梯","hasBarrierFreeElevator");

    }


    /**
     * 读取excel数据
     *
     * @throws Exception
     */
    public static List<Map<String , String>> readExcelInfo(MultipartFile file) throws Exception {
        /*
         * workbook:工作簿,就是整个Excel文档
         * sheet:工作表
         * row:行
         * cell:单元格
         */

        //BufferedWriter bw = new BufferedWriter(new FileWriter(new File(url)));
        //支持excel2003、2007
        //创建excel文件对象
        //创建输入流对象
        InputStream is = file.getInputStream();
        checkExcelVaild(file.getOriginalFilename());
        Workbook workbook = getWorkBook(is, file.getOriginalFilename());
        //获取Sheet数量
        int sheetNum = workbook.getNumberOfSheets();
        //创建二维数组保存所有读取到的行列数据，外层存行数据，内层存单元格数据
        List<Map<String , String>> objectMapList = new ArrayList<>();
        //遍历工作簿中的sheet,第一层循环所有sheet表
        for (int index = 0; index < sheetNum; index++) {
            Sheet sheet = workbook.getSheetAt(index);
            if (sheet == null) {
                continue;
            }
            //如果当前行没有数据跳出循环，第二层循环单sheet表中所有行
            //标题头
            Row headRow = sheet.getRow(0);
            if(null == headRow){
                continue;
            }

            Map<Integer , String > indexAttributeMap = new HashMap<>(headRow.getLastCellNum());

            for (int cellIndex = 0; cellIndex < headRow.getLastCellNum(); cellIndex++) {
                Cell cell = headRow.getCell(cellIndex);
                String title = getCellValue(cell);

                if(YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.containsKey(title)){
                    indexAttributeMap.put(cellIndex , YW_DEV_BUILDING_USED_NAME_ATTRIBUTE_MAP.get(title));
                }
            }


            for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
                Row row = sheet.getRow(rowIndex);
                //根据文件头可以控制从哪一行读取，在下面if中进行控制
                if (row == null) {
                    continue;
                }
                //遍历每一行的每一列，第三层循环行中所有单元格
                Map<String , String> objectMap = new HashMap<>();
                for (int cellIndex = 0; cellIndex < row.getLastCellNum(); cellIndex++) {
                    Cell cell = row.getCell(cellIndex);
                    String cellValue = getCellValue(cell);

                    String attribute = indexAttributeMap.get(cellIndex);
                    objectMap.put(attribute , cellValue);
                }
                objectMapList.add(objectMap);
            }

        }
        is.close();
        return objectMapList;
    }

    /**
     * 获取单元格的数据,暂时不支持公式
     */
    public static String getCellValue(Cell cell) {
        CellType cellType = cell.getCellType();
        String cellValue = "";
        if (cell == null || "".equals(cell.toString().trim())) {
            return null;
        }

        if (cellType == CellType.STRING) {
            cellValue = cell.getStringCellValue().trim();
            return cellValue = StringUtil.isEmpty(cellValue) ? "" : cellValue;
        }
        if (cellType == CellType.NUMERIC) {
            //判断日期类型
            if (DateUtil.isCellDateFormatted(cell)  ) {
                cellValue = DateUtils.format(cell.getDateCellValue().getTime() , "yyyy-MM-dd");
            } else {  //否
                cellValue = new DecimalFormat("#.######").format(cell.getNumericCellValue());
            }
            return cellValue;
        }
        if (cellType == CellType.BOOLEAN) {
            cellValue = String.valueOf(cell.getBooleanCellValue());
            return cellValue;
        }
        return null;

    }

    /**
     * 判断excel的版本，并根据文件流数据获取workbook
     * @throws Exception
     */
    public static Workbook getWorkBook(InputStream is, String fileName) throws Exception {

        Workbook workbook = null;
        if (fileName.endsWith(EXCEL_XLS)) {
            workbook = new HSSFWorkbook(is);
        } else if (fileName.endsWith(EXCEL_XLSX)) {
            workbook = new XSSFWorkbook(is);
        }

        return workbook;
    }

    /**
     * 校验文件是否为excel
     *
     * @throws Exception
     */
    public static void checkExcelVaild(String fileName) throws Exception {
        String message = "该文件是EXCEL文件！";
        if  ((!fileName.endsWith(EXCEL_XLS) && !fileName.endsWith(EXCEL_XLSX))) {
            message = "文件不是Excel";
            throw new Exception(message);
        }
    }

}
