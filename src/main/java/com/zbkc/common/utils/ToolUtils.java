package com.zbkc.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * 文件工具类
 * @author gmding
 * @date 2021-08-10
 */
@Slf4j
@Component
public class ToolUtils {

    private final String regex ="[0-9A-Za-z]{5,20}$";
    private final String regex1 ="[0-9A-Za-z]{6,20}$";

    //24小时
    private final Long HOURS_24 = 60*24*60*60L;

    @Autowired
    private RedisUtil redisUtil;
    /**
     * 正则表达式 用户名5~20位数字或字母
     *
     * */
    public Boolean RegName(String username){
        if (username.matches( regex )){
            return true;
        }
        return false;

    }

    public Boolean RegPwd(String password){
        if (password.matches( regex1 )){
            return true;
        }
        return false;

    }

    /**
     *
     * 物业编号生成规则
     * */
    public String getPropertyCode(){
        String code="NS"+new DateUtil().getYear()+new DateUtil().getMonth();
        String num= redisUtil.get(code);
        if (num==null||"".equals(num)){
            redisUtil.set(code,"1",60*24*60*60);
            return code+"0001";
        }else {
            int count=Integer.valueOf(num)+1;
            redisUtil.set(code,""+count,60*24*60*60);
            return code+getCode(count);
        }
    }

    /**
     * 项目编号生成 移交用房
     * @return 移交用房项目编号
     */
    public String projectCodeGeneratorYJYF(){
        //规则 日期+四位编号0001
        DateUtil dateUtil = new DateUtil();

        String code = dateUtil.getYear() + dateUtil. getMonth() + dateUtil.getDayOfMonth();

        String num = redisUtil.get(code);

        if (num==null||"".equals(num)){
            redisUtil.set(code,"1", HOURS_24);
            return code+"001";
        }else {
            int count=Integer.valueOf(num)+1;
            redisUtil.set(code , String.valueOf(count) , HOURS_24);
            return code+getCode2(count);
        }
    }
    /**
     * 移交用房受理号生成
     * @return 移交用房受理号
     */
    public String acceptCodeGeneratorYJYF(){
        DateUtil dateUtil = new DateUtil();
        String code = "YFYJNS" + dateUtil.getYear() + dateUtil.getMonth() + dateUtil.getDayOfMonth();
        String num= redisUtil.get(code);

        if (num==null||"".equals(num)){
            redisUtil.set(code,"1", HOURS_24);
            return code+"001";
        }else {
            int count=Integer.valueOf(num)+1;
            redisUtil.set(code , String.valueOf(count) , HOURS_24);
            return code+getCode2(count);
        }
    }

    private String getCode2(int count){
        if (count<10){
            return "00"+count;
        }else if (count<100&&count>=10){
            return "0"+count;
        }else if (count>=100&&count<1000){
            return ""+count;
        }else {
            return str2HexStr(count);
        }
    }


    private String getCode(int count){
        if (count<10){
            return "000"+count;
        }else if (count<100&&count>=10){
            return "00"+count;
        }else if (count>=100&&count<1000){
            return "0"+count;
        }else if (count>=1000&&count<10000){
            return ""+count;
        }else {
            return str2HexStr(count);
        }
    }

    /**
     * 字符串转换成为16进制(无需Unicode编码)
     * @param str
     * @return
     */
    public String str2HexStr(int str) {

        String str1=Integer.toHexString(str).toUpperCase();
        for (int i=0;i<4;i++){
            if (i>str1.length()){
                str1="0"+str1.toString().trim();
            }
        }
        return str1;
    }

    /**
     * 将时间串转成Timestamp类型
     * @param str yyyy-MM-dd
     */
    public Timestamp strFormatTimetamp(String str){
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setLenient(false);
        Timestamp ts = null;
        try {
            ts= new Timestamp(format.parse(str).getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ts;
    }


    public static void main(String[] arg){

        String str="2021-08-18";
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setLenient(false);
        Timestamp ts = null;
        try {
            ts= new Timestamp(format.parse(str).getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*System.out.println(Integer.toHexString(12000).toUpperCase());
        System.out.println(new ToolUtils().getCode(10000));
        System.out.println(new ToolUtils().str2HexStr(120000));*/

    }


}
