package com.zbkc.common.utils;

import org.apache.velocity.exception.MathException;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * 前端传回来的Long类型转化为时间戳
 * @author yangyan
 * @date 2021/1/16
 */
@Component
public class TimeUtils {
    // 将long类型转为时间戳
    public static Timestamp LongToDate(Long lo) {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        return timestamp;
    }

    public static  Long cluDay(String startRentDat,String endRentDate){
        Long start=Long.parseLong(startRentDat);
        Long end =Long.parseLong(endRentDate);
        Long day=(start-end)/24/60/1000;
         return day;
    }


    /**
     * 将指定的日期字符串转换成日期
     * @param dateStr 日期字符串
     * @param pattern 格式
     * @return 日期对象
     */
    public static Date parseDate(String dateStr, String pattern)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date date;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            throw  new RuntimeException("日期转化错误");
        }

        return date;
    }

    /**
     *
     * @param startDate
     * @param endDate
     * @return 天数差
     */
    public static Long getBetweenDays(String startDate,String endDate){
        // 获取日期
        Date start = TimeUtils.parseDate(startDate, "yyyy-MM-dd");
        Date end = TimeUtils.parseDate(endDate, "yyyy-MM-dd");

        // 获取相差的天数
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(start);
        long startTime = calendar.getTimeInMillis();
        calendar.setTime(end);
        long endTime = calendar.getTimeInMillis();
        long betweenDays =  (endTime -startTime) / (1000L*3600L*24L);
        return betweenDays;
    }

    /**
     * 将Date类型日期转化成字符串日期
     * @param date
     * @return
     */
    public static String getDateString(Date date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(date);
        return format;
    }

    /**
     * 获取两个日期相差的年份数，不满一年算一年
     * @param startDate
     * @param endDate
     * @return
     */
    public static Long getBetweenYears(String startDate,String endDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar bef = Calendar.getInstance();
        Calendar aft = Calendar.getInstance();
        try {
            bef.setTime(sdf.parse(startDate));
            aft.setTime(sdf.parse(endDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long surplus = aft.get(Calendar.DATE) - bef.get(Calendar.DATE);
        long result = aft.get(Calendar.MONTH) - bef.get(Calendar.MONTH);
        long year = aft.get(Calendar.YEAR) - bef.get(Calendar.YEAR);
        if (year < 0) {
            try {
                throw new MathException("请将大日期放后面");
            } catch (MathException e) {
                e.printStackTrace();
            }
        }
        if (result < 0) {
            result = 1;
        } else if (result == 0) {
            result = surplus <= 0 ? 0 : 1;
        } else {
            result = 1;
        }

        return year + result;
    }

    public static void main(String[] args) {
//        System.out.println(System.currentTimeMillis());

//        System.out.println(cluDay("1631499107","1631635200"));
//
//        String dateStr1 = "2018-12-27 17:07:07";
//        String dateStr2 = "2018-12-31 00:00:00";
//        System.out.println(TimeUtils.getBetweenDays("2018-12-27 17:07:07", "2018-12-31 00:00:00"));
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println(simpleDateFormat.format(date));
//        System.out.println(TimeUtils.getDateString(new Date()));
        String str1 = "2012-02-06 00:00:00";
        String str2 = "2012-01-04 00:00:00";
        System.out.println(getBetweenYears(str1, str2));

    }
}
