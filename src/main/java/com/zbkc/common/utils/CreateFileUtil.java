package com.zbkc.common.utils;


import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.model.vo.FileVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * 文件工具类
 * @author gmding
 * @date 2021-8-9
 */
@Component
@Slf4j
public class CreateFileUtil {

    @Value( "${file.win-name}")
    private String winUrl;
    @Value( "${file.linux-name}" )
    private String linuxUrl;
    @Value( "${file.url}" )
    private String url;

    @Resource
    private DesUtils desUtils;

    /**
     * 创建文件目录
     *
     * */
    public String createFile(){

        String fileDir=getRandomString(2)+"/"+getRandomString(2)+"/"+getRandomString(2)+"/"+getRandomString(2)+"/"+getRandomString(2);

        String fileName=fileDir;
        if(isWindows()){
            fileDir=winUrl+"/"+fileDir.toUpperCase();
        }else {
            fileDir=linuxUrl+"/"+fileDir.toUpperCase();
        }

        try {
            File file = new File(fileDir);
            if (!file.isDirectory()) {
                /**
                 * mkdir() 创建目录，例如：D:/a
                 * mkdirs() 递归创建目录，例如：D:/a/b/c
                 */
                file.mkdirs();
            }
        }catch (Exception e){
            log.error( "创建目录失败:{}",e.toString() );
        }
        return fileName.toUpperCase();
    }


    public boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }

    //length用户要求产生字符串的长度
    public String getRandomString(int length){
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<length;i++){
            int number=random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    public FileVo saveFile(MultipartFile file){
        FileVo vo=new FileVo();
        if (file.isEmpty()){
            vo.setCode( ErrorCodeEnum.FILE_ERR.getCode() );
            vo.setMsg(ErrorCodeEnum.FILE_ERR.getErrMsg()  );
            return vo;
        }
        String beforeName=file.getOriginalFilename();//原文件名
        String filename = file.getOriginalFilename(); //获取上传文件原来的名称
        String filePath = createFile();
        String suffixname = filename.substring(filename.lastIndexOf("."));//后缀
        filename = desUtils.getUuid() + suffixname;//文件上传后重命名数据库存储

        String fileDir=filePath;
        if(isWindows()){
            fileDir=winUrl+"/"+fileDir.toUpperCase();
        }else {
            fileDir=linuxUrl+"/"+fileDir.toUpperCase();
        }

        File localFile = new File(fileDir+"/"+filename);
        try {
            file.transferTo(localFile); //把上传的文件保存至本地
            vo.setCode( ErrorCodeEnum.SUCCESS.getCode() );
            vo.setMsg( ErrorCodeEnum.SUCCESS.getErrMsg() );
            vo.setFileUrl(url+filePath+"/"+filename);
            vo.setFileName( beforeName );
            vo.setFilePath(filePath+"/"+filename);
            vo.setFileSize(String.valueOf(file.getSize()));
        }catch (IOException e){
            log.error( "上传文件失败:{}",e.toString());
            vo.setCode( ErrorCodeEnum.FILE_ERR2.getCode() );
            vo.setMsg(ErrorCodeEnum.FILE_ERR2.getErrMsg()  );
            return vo;
        }

        return vo;
    }

    /**
     * 文件删除
     * @param fileName 文件目录+名称
     * @return
     */
    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + fileName + "成功！");
                return true;
            } else {
                System.out.println("删除单个文件" + fileName + "失败！");
                return false;
            }
        } else {
            System.out.println("删除单个文件失败：" + fileName + "不存在！");
            return false;
        }
    }



}
