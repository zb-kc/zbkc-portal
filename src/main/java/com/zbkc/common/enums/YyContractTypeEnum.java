package com.zbkc.common.enums;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/12/16
 */
public enum YyContractTypeEnum {

    /**
     * 合同的模板类型枚举
     * 1产业用房租赁合同3+2（有经济贡献率）;
     * 2产业用房租赁合同3+2（无经济贡献率）;
     * 3产业用房租赁合同3年及3年以下;
     * 4商业租赁合同；
     * 5住宅租赁合同；
     * 6南山软件租赁合同
     *
     */
    CONTRACT_TYPE_1(1, "产业用房租赁合同3+2（有经济贡献率）"),
    CONTRACT_TYPE_2(2, "产业用房租赁合同3+2（无经济贡献率）"),
    CONTRACT_TYPE_3(3, "产业用房租赁合同3年及3年以下"),
    CONTRACT_TYPE_4(4, "商业租赁合同"),
    CONTRACT_TYPE_5(5, "住宅租赁合同"),
    CONTRACT_TYPE_6(6, "南山软件租赁合同"),
            ;

    private Integer typeCode;
    private String typeName;

    YyContractTypeEnum(Integer typeCode, String typeName) {
        this.typeCode = typeCode;
        this.typeName = typeName;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(Integer typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
