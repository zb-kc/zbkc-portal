package com.zbkc.common.enums;

/**
 * 工作流 业务类型枚举类
 * @author ZB3436.xiongshibao
 * @date 2021/10/11
 */
public enum ActivitiBusinessTypeEnum {
    //待办业务
    TODO("todo"),
    //所有业务
    ALL("all");

    private String businessType;

    ActivitiBusinessTypeEnum(String businessType) {
        this.businessType = businessType;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }
}
