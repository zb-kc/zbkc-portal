package com.zbkc.common.enums;

/**
 * 状态码描述管理
 * @author gmding
 * @date 2021-7-16
 * */

public enum ErrorCodeEnum{

    //成功
    SUCCESS(0,"success"),
    //未授权认证
    UNAUTHORIZED(401,"未授权认证"),
    //未找到该资源
    RESOURCE_NOT_FOUND(1001,  "未找到该资源"),
    //异地登录
    OFFSITELOGIN(1002,"token过期，重新刷新"),
    //用户不存在
    USER_NOT_FOUND(1003,"用户名不存在或者已被禁用"),
    //密码错误
    USERNAMEORPASSWORD_INPUT_ERROR(1004,"密码或者用户名输入错误"),
    TOKEN_NOT_FOUND(1005,"token未发现"),
//    TOKEN_INVALID(1006,"token无效"),
    SMS_ERROR(1007,"短信发送失败"),
    SMS_SEND_ERROR_BUSINESS_LIMIT_CONTROL(1008,"短信发送频率超限"),
    EMAIL_SEND_FAILD(1009,"邮件发送失败"),
    ROLE_NO_MENUS(1010,"该角色还未赋权"),
    NULLERROR(1011,"传入数据为空"),
    ALREAD_EXIT(1012,"已经存在相同的名称"),
    FILE_ERR(1013,"上传文件为空"),
    FILE_ERR2(1014,"上传文件失败"),
    REGULARNOTACCORD(1015,"不符合正则"),
    INCHANGEWERECHANGE(1016,"不可变参数发生改变"),
    USERID_NOT_FOUND(1017,"用户id为空"),
    ERROR_BACK(1018,"数据改动失败"),
    NAME_ALREADY_EXISTS (1019,"名称已存在"),
    AGENCY_TYPE_ERROR (1020,"业务申请中代办类型不正确"),
    AGENCY_APPLICATION_NOT_FOUND (1021,"业务申请记录不存在"),
    ACTIVITI_TASK_NOT_FOUND (1022,"工作流任务节点不存在"),
    PICTURE_PROCESS_NOT_FOUND (1023,"流程未开启，流程图片不存在"),
    CONTRACT_NOT_FOUND (1024,"合同信息不存在"),
    SOCIAL_UNITFORM_CREDIT_CODE_ERROR (1025,"社会统一信用代码不正确"),
    ID_ERROR (1026,"记录不存在"),
    PAGE_NUMBER_ERROR (1027,"请输入页码"),
    DUPLICATE_ERROR (1028,"请勿重复操作"),
    DOWNLAND_FILE_ERROR (1029,"下载文件异常,关联ID不正确"),
    CANEL_ERROR_PROGRESSING (1030,"撤回申请失败，审核流程已启动"),
    SERVER_ERROR (1031,"服务器错误!"),
    COLLECTION_HAS (1032,"已收藏!请勿重复操作"),
    COLLECTION_UNDO (1033,"已取消收藏!请勿重复操作"),
    PERMISSIONS_ERROR (1034,"操作失败!未经授权不可访问"),
    FORM_VALIDATOR_ERR (1035,"表单校验失败"),
    ID_IS_NULL (1036,"ID字段缺失，请输入")
    ;


    private int code;

    private String errMsg;

    ErrorCodeEnum(int code,  String errMsg) {

        this.code = code;
        this.errMsg = errMsg;
    }

    public int getCode() {
        return code;
    }

    public String getErrMsg() {
        return errMsg;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "code=" + code +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }

    /**
     * 自己定义一个静态方法,通过code返回枚举常量对象
     * @param code
     * @return
     */
    public static ErrorCodeEnum getValue(int code){
        for (ErrorCodeEnum  errorCodeEnum: values()) {
            if(errorCodeEnum.getCode() == code){
                return  errorCodeEnum;
            }
        }
        return null;
    }


}