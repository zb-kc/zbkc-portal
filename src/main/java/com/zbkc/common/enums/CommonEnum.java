package com.zbkc.common.enums;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/12/16
 */
public final class CommonEnum {

    /**
     * 业务办理表
     * 申请业务类型 1产业用房租赁 2商业用房租赁 3住宅用房租赁 4南山软件园租赁
     */
    public static final String BUSINESS_TYPE_1 = "1";
    public static final String BUSINESS_TYPE_2 = "2";
    public static final String BUSINESS_TYPE_3 = "3";
    public static final String BUSINESS_TYPE_4 = "4";
    /**
     * 是否经济共享承诺
     * 1 是
     * 2 否
     */
    public static final Integer IS_ECONOMIC_COMMIT_1 = 1;
    public static final Integer IS_ECONOMIC_COMMIT_2 = 2;

    /**
     * 流程状态 0刚授权 1企业申请 2企业撤回
     */
    public static final Byte PROCESS_NEW_RECORD = 0;
    public static final Byte PROCESS_APPLY = 1;
    public static final Byte PROCESS_APPLY_UNDO = 2;
    /**
     * 申请来源：1现场申请（管理端合同新增） 2线上申请（门户）
     */
    public static final String SIGN_SOURCE_SIT_APPLY = "1";
    public static final String SIGN_SOURCE_ONLINE_APPLY = "2";

    /**
     * 0未审核
     * 1审核通过
     * 2有问题
     */
    public static final String FILE_STATUS_TODO = "0";
    public static final String FILE_STATUS_OK = "1";
    public static final String FILE_STATUS_ERROR = "2";

    /**
     * 1 收藏
     * 2 取消收藏
     */
    public static final Byte WY_COLLECT = 1;
    public static final Byte WY_COLLECT_UNDO = 2;

}
