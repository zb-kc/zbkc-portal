package com.zbkc.common.enums;

/**
 * 合同签署申请 附件名称
 * @author ZB3436.xiongshibao
 * @date 2021/10/21
 */
public enum ContractFileNameEnum {

    /**
     * 合同签署申请 附件名称
     */
    File_1(1,	"入驻通知书"	),
    File_2(2,	"营业执照复印件"	),
    File_3(3,	"法人身份证复印件"	),
    File_4(4,	"股东证明书"	),
    File_5(5,	"法人代表证明书"	),
    File_6(6,	"法人授权委托书"	),
    File_7(7,	"经办人身份证复印件"	),
    File_8(8,	"南山区产业用房建设和管理工作领导小组会议纪要"	),
    File_9(9,	"房屋建筑面积总表，房屋建筑面积分户汇总表及房屋建筑面积分户位置图"	),
    File_10(10,	"有经济贡献率的企业的经济贡献承诺书"	),
    File_11(11,	"南山区政策性产业用房入驻通知书"	),
    File_12(12,	"会议纪要"	),
    File_13(13,	"租金价格评估报告"	),
    File_14(14,	"身份证复印件"	),
    File_15(15,	"南山数字文化产业基地入驻通知书"	),
    File_16(16,	"合同"	),
    File_17(17,	"续签通知书"	),
    ;

    private int id;
    private String fileName;

    ContractFileNameEnum(int id, String fileName) {
        this.id = id;
        this.fileName = fileName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
