package com.zbkc.configure;

/**
 * 过滤路径
 * @author gmding
 * @date 2021-7-16
 * */

public class BasePathPatterns {


    public final static  String urlPath="http://192.168.1.76:10005/api/v1/zbkc/";

    /**
     * 过滤拦截器路径,适合正则
     * @apiParam string
     *
     * */
    public final static String[] URL = {
            "/api/v1/zbkc-portal/text/login(.*)",
            //"/api/v1/zbkc-portal/text(.*)",
            "/api/v1/zbkc-portal/swagger-resources(.*)",
            "/api/v1/zbkc-portal/error",
            "/api/v1/zbkc-portal/userInfo/login",
            "/api/v1/zbkc-portal/userInfo/(.*)",
            "/api/v1/zbkc-portal/userInfo/refreshToken",
            "/api/v1/zbkc-portal/sms/(.*)",
            "/api/v1/zbkc-portal/(.*)"

    };

}
