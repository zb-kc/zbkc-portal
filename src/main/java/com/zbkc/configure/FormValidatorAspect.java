package com.zbkc.configure;

import com.zbkc.common.utils.FormValidatorUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 表单参数校验 aop切面
 * @author ZB3436.xiongshibao
 * @date 2021/11/30
 */
@Aspect
@Component
public class FormValidatorAspect {

    @Resource
    private FormValidatorUtils formValidatorUtils;

    /**
     * 定义切面要切的方法为所有的带这个注解的方法
     */
    @Pointcut("@annotation(org.springframework.web.bind.annotation.PostMapping)")
    public void formValidator(){

    }

    /**
     * 切面逻辑
     * @param point 切点
     */
    @Before("formValidator()")
    public void paramValidateBefore(JoinPoint point) {
        Object[] args = point.getArgs();

        for (Object arg : args) {
            if(null == arg){
                continue;
            }
            formValidatorUtils.validate(arg);
        }
    }

}
