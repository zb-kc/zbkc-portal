package com.zbkc.configure;

import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.model.vo.ResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *  异常处理器
 *  @author gmding
 *  @date 2021-7-16
 * */

@ControllerAdvice
@Slf4j
public class ExceptionHandler {

    /**
     * 处理BusinessException异常
     * @apiParam e
     * @return
     */
    @org.springframework.web.bind.annotation.ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public ResponseVO busin(BusinessException e) {
        log.info("data:{}",new Gson().toJson(e));
        return  new ResponseVO(e.getCode(), e.getErrMsg(),null);
    }

    /**
     * 通用异常处理器
     * @param e
     * @return
     */
    @org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseVO exception(Exception e) {
        log.info("data:{}",new Gson().toJson(e));
        e.printStackTrace();
        return  new ResponseVO(ErrorCodeEnum.SERVER_ERROR.getCode(), ErrorCodeEnum.SERVER_ERROR.getErrMsg(),e.getMessage());
    }
}